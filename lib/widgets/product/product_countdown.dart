import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class CountDownTimer extends StatefulWidget {
  final DateTime datetime;

  CountDownTimer({@required this.datetime});

  @override
  _CountDownTimerState createState() => _CountDownTimerState();
}

class _CountDownTimerState extends State<CountDownTimer>
    with TickerProviderStateMixin {
  AnimationController _controller;

  String get timerString {
    Duration duration = _controller.duration * _controller.value;
    if (duration.inHours + duration.inMinutes + duration.inSeconds == 0) {
      return 'Finished';
    } else if (duration.inDays > 0) {
      return '${duration.inDays}D:${(duration.inHours % 24).toString().padLeft(2, '0')}:${(duration.inMinutes % 60).toString().padLeft(2, '0')}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
    }
    return '${(duration.inHours % 24).toString().padLeft(2, '0')}:${(duration.inMinutes % 60).toString().padLeft(2, '0')}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  @override
  void initState() {
    super.initState();
    DateTime datetimeUtc = widget.datetime.toUtc();
    int duration = DateTime.now().toUtc().difference(datetimeUtc).inSeconds;
    if (duration > 0) {
      duration = 0;
    } else {
      duration = duration.abs();
    }
    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: duration),
    );
    _controller.reverse(
        from: _controller.value == 0.0 ? 1.0 : _controller.value);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              child: Row(
                children: <Widget>[
                  Icon(
                    MaterialCommunityIcons.clock_fast,
                    color: Colors.deepPurple,
                    size: 24.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 4.0),
                    child: Text(
                      timerString,
                      style: TextStyle(
                          fontSize: 15.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.blueGrey[800]),
                    ),
                  )
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
