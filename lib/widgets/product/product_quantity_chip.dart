import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

prizeChip(int quantity) {
  return Chip(
      labelPadding: quantity > 99
          ? EdgeInsets.symmetric(horizontal: 4.0, vertical: 1.0)
          : null,
      backgroundColor: Colors.lightGreen,
      label: Row(
        children: <Widget>[
          Icon(
            MaterialCommunityIcons.gift,
            color: Colors.white,
            size: 15.0,
          ),
          Container(
            margin: EdgeInsets.only(left: 4.0),
            child: Text(
              quantity > 99 ? '+99' : quantity.toString(),
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.w700),
            ),
          ),
        ],
      ));
}
