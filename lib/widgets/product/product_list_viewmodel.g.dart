// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_list_viewmodel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ProductListViewModel extends ProductListViewModel {
  @override
  final BuiltList<Product> products;

  factory _$ProductListViewModel(
          [void Function(ProductListViewModelBuilder) updates]) =>
      (new ProductListViewModelBuilder()..update(updates)).build();

  _$ProductListViewModel._({this.products}) : super._() {
    if (products == null) {
      throw new BuiltValueNullFieldError('ProductListViewModel', 'products');
    }
  }

  @override
  ProductListViewModel rebuild(
          void Function(ProductListViewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProductListViewModelBuilder toBuilder() =>
      new ProductListViewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProductListViewModel && products == other.products;
  }

  @override
  int get hashCode {
    return $jf($jc(0, products.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProductListViewModel')
          ..add('products', products))
        .toString();
  }
}

class ProductListViewModelBuilder
    implements Builder<ProductListViewModel, ProductListViewModelBuilder> {
  _$ProductListViewModel _$v;

  ListBuilder<Product> _products;
  ListBuilder<Product> get products =>
      _$this._products ??= new ListBuilder<Product>();
  set products(ListBuilder<Product> products) => _$this._products = products;

  ProductListViewModelBuilder();

  ProductListViewModelBuilder get _$this {
    if (_$v != null) {
      _products = _$v.products?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProductListViewModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProductListViewModel;
  }

  @override
  void update(void Function(ProductListViewModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProductListViewModel build() {
    _$ProductListViewModel _$result;
    try {
      _$result =
          _$v ?? new _$ProductListViewModel._(products: products.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'products';
        products.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProductListViewModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
