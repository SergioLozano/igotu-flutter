import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/svg.dart';
import 'package:igotu/models/product.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/products/product_actions.dart';
import 'package:igotu/routes.dart';
import 'package:igotu/widgets/product/product_quantity_chip.dart';
import 'package:igotu/utils/utils.dart';

class ProductListItem extends StatelessWidget {
  const ProductListItem({
    @required Product product,
    Key key,
  })  : _product = product,
        super(key: key);

  final Product _product;

  showProduct(context) {
    StoreProvider.of<AppState>(context)
        .dispatch(SelectProductByIdAction(productId: _product.id));
    Navigator.of(context).pushNamed(Routes.product);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
      semanticContainer: true,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: InkWell(
        onTap: () => showProduct(context),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: MediaQuery.of(context).size.width / 2,
              child: Stack(
                children: <Widget>[
                  Positioned.fill(
                    child: Ink.image(
                      image: CachedNetworkImageProvider(_product.coverUrl),
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
              child: Column(
                children: <Widget>[
                  Container(
                    child: Text(
                      _product.name,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        prizeChip(_product.quantity),
                        Row(
                          children: <Widget>[
                            Container(
                              width: 28,
                              height: 28,
                              margin: EdgeInsets.only(right: 1.0),
                              child:
                                  SvgPicture.asset('assets/icons/tickets.svg'),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 2.5),
                              child: Text(
                                formatTicketsValues(_product.tickets),
                                style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    color: Colors.black54),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
