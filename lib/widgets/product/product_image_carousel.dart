import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:igotu/widgets/common/custom_image.dart';

class ProductCarousel extends StatefulWidget {
  final List<String> images;

  ProductCarousel({@required this.images});

  @override
  _ProductCarouselState createState() => _ProductCarouselState();
}

class _ProductCarouselState extends State<ProductCarousel> {
  int _current = 0;

  buildCarouselItems() {
    return widget.images.map((i) {
      return Builder(
        builder: (BuildContext context) {
          return Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(color: Colors.white),
              child: cachedNetworkImage(i));
        },
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(bottom: 0.0),
          child: CarouselSlider(
            items: buildCarouselItems(),
            autoPlay: true,
            aspectRatio: 2.0,
            pauseAutoPlayOnTouch: Duration(seconds: 10),
            onPageChanged: (index) {
              setState(() {
                _current = index;
              });
            },
          ),
        ),
        Positioned(
          bottom: 0.0,
          left: 0.0,
          right: 0.0,
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: widget.images.map((image) {
                return Container(
                  width: _current == widget.images.indexOf(image) ? 8.0 : 4.0,
                  height: _current == widget.images.indexOf(image) ? 8.0 : 4.0,
                  margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == widget.images.indexOf(image)
                          ? Theme.of(context).accentColor.withOpacity(0.3)
                          : Theme.of(context).primaryColor.withOpacity(0.3)),
                );
              }).toList()),
        ),
      ],
    );
  }
}
