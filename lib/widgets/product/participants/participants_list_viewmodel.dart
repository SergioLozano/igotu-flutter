import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:igotu/models/participant.dart';
import 'package:igotu/redux/app_state.dart';
import "package:redux/redux.dart";

// ignore: prefer_double_quotes
part 'participants_list_viewmodel.g.dart';

abstract class ParticipantsListViewModel
    implements
        Built<ParticipantsListViewModel, ParticipantsListViewModelBuilder> {
  BuiltList<Participant> get participants;

  ParticipantsListViewModel._();

  factory ParticipantsListViewModel(
          [void Function(ParticipantsListViewModelBuilder) updates]) =
      _$ParticipantsListViewModel;

  static ParticipantsListViewModel fromStore(Store<AppState> store) {
    final user = store.state.user;
    final participants = store.state.participants.where((p) => p.uid != user.uid).toList();

    return ParticipantsListViewModel(
        (p) => p..participants.addAll(participants));
  }

  //TODO: implement hasData
}
