import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_lottie/flutter_lottie.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:igotu/igotu_localization.dart';
import 'package:igotu/models/participant.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/widgets/common/bubble.dart';
import 'package:igotu/widgets/product/participants/participant_card.dart';
import 'package:igotu/widgets/product/participants/participants_list_viewmodel.dart';

class ParticipantsList extends StatefulWidget {
  ParticipantsList();

  @override
  _ParticipantsListState createState() => _ParticipantsListState();
}

class _ParticipantsListState extends State<ParticipantsList> {
  StreamController<double> newProgressStream;
  LottieController controller;

  @override
  void initState() {
    super.initState();
    newProgressStream = new StreamController<double>();
  }

  @override
  void dispose() {
    newProgressStream.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double pixelRatio = MediaQuery.of(context).devicePixelRatio;
    double px = 1 / pixelRatio;

    BubbleStyle styleSomebody = BubbleStyle(
      nip: BubbleNip.leftBottom,
      color: Colors.white,
      elevation: 1 * px,
      margin: BubbleEdges.only(bottom: 52.0),
      alignment: Alignment.topLeft,
    );

    return StoreConnector<AppState, ParticipantsListViewModel>(
      converter: ParticipantsListViewModel.fromStore,
      distinct: true,
      builder: (context, vm) {
        if (vm.participants.isNotEmpty) {
          return ListView.separated(
            reverse: true,
            shrinkWrap: true,
            itemCount: vm.participants.length,
            itemBuilder: (context, index) {
              final participant = vm.participants[index];
              return _buildParticipant(participant);
            },
            separatorBuilder: (context, index) {
              return Divider(
                height: 0,
              );
            },
          );
        } else {
          return Container(
              padding: EdgeInsets.symmetric(horizontal: 48.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                      width: MediaQuery.of(context).size.width / 3,
                      height: MediaQuery.of(context).size.width / 3,
                      child: LottieView.fromFile(
                        filePath: "assets/animations/dino-dance.json",
                        autoPlay: true,
                        loop: true,
                        onViewCreated: onViewCreated,
                      )),
                  Expanded(
                    flex: 1,
                    child: Bubble(
                        style: styleSomebody,
                        child: Text(
                            IgotuLocalizations.of(context).emptyParticipantList,
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.w600, color: Theme.of(context).primaryColor.withOpacity(0.8)))),
                  )
                ],
              ));
        }
      },
    );
  }

  Widget _buildParticipant(Participant participant) {
    return ParticipantCard(
      participant: participant,
    );
  }

  void onViewCreated(LottieController controller) {
    this.controller = controller;
    newProgressStream.stream.listen((double progress) {
      this.controller.setAnimationProgress(progress);
    });
  }
}
