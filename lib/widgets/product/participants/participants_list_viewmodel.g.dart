// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'participants_list_viewmodel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ParticipantsListViewModel extends ParticipantsListViewModel {
  @override
  final BuiltList<Participant> participants;

  factory _$ParticipantsListViewModel(
          [void Function(ParticipantsListViewModelBuilder) updates]) =>
      (new ParticipantsListViewModelBuilder()..update(updates)).build();

  _$ParticipantsListViewModel._({this.participants}) : super._() {
    if (participants == null) {
      throw new BuiltValueNullFieldError(
          'ParticipantsListViewModel', 'participants');
    }
  }

  @override
  ParticipantsListViewModel rebuild(
          void Function(ParticipantsListViewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ParticipantsListViewModelBuilder toBuilder() =>
      new ParticipantsListViewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ParticipantsListViewModel &&
        participants == other.participants;
  }

  @override
  int get hashCode {
    return $jf($jc(0, participants.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ParticipantsListViewModel')
          ..add('participants', participants))
        .toString();
  }
}

class ParticipantsListViewModelBuilder
    implements
        Builder<ParticipantsListViewModel, ParticipantsListViewModelBuilder> {
  _$ParticipantsListViewModel _$v;

  ListBuilder<Participant> _participants;
  ListBuilder<Participant> get participants =>
      _$this._participants ??= new ListBuilder<Participant>();
  set participants(ListBuilder<Participant> participants) =>
      _$this._participants = participants;

  ParticipantsListViewModelBuilder();

  ParticipantsListViewModelBuilder get _$this {
    if (_$v != null) {
      _participants = _$v.participants?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ParticipantsListViewModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ParticipantsListViewModel;
  }

  @override
  void update(void Function(ParticipantsListViewModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ParticipantsListViewModel build() {
    _$ParticipantsListViewModel _$result;
    try {
      _$result = _$v ??
          new _$ParticipantsListViewModel._(participants: participants.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'participants';
        participants.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ParticipantsListViewModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
