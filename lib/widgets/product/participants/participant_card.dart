import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:igotu/models/participant.dart';
import 'package:igotu/utils/utils.dart';

class ParticipantCard extends StatelessWidget {
  const ParticipantCard({Key key, @required Participant participant})
      : _participant = participant,
        super(key: key);

  final Participant _participant;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            children: <Widget>[
              CircleAvatar(
                radius: 14.0,
                backgroundColor: Colors.transparent,
                child: CachedNetworkImage(
                  imageUrl: _participant.image,
                  errorWidget: (context, url, error) =>
                      Image.asset('assets/images/avatars/bot-4.png'),
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 0.0, left: 8.0),
                child: Text(
                  _participant.username,
                  style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueGrey[800]),
                ),
              )
            ],
          ),
          Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 2.0, right: 4.0),
                child: Text(
                  formatTicketsValues(_participant.tickets),
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      color: Theme.of(context).primaryColor.withOpacity(0.8)),
                ),
              ),
              Container(
                width: 18,
                height: 18,
                margin: EdgeInsets.only(right: 2.0),
                child: SvgPicture.asset('assets/icons/tickets.svg'),
              ),
            ],
          )
        ],
      ),
    );
  }
}
