import 'package:flutter/material.dart';
import 'package:flutter_fluid_slider/flutter_fluid_slider.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/bid/bid_actions.dart';
import 'package:igotu/widgets/common/progress.dart';

class TicketsDialog extends StatefulWidget {
  final String productId;
  final int userTickets;


  TicketsDialog({
    @required this.productId,
    @required this.userTickets,
  });

  @override
  _TicketsDialogState createState() => _TicketsDialogState(
    productId: this.productId, 
    userTickets: this.userTickets,
  );
}

class _TicketsDialogState extends State<TicketsDialog> {
  final String productId;
  final int userTickets;

  double _amount = 1.0;
  bool _enabled = true;

  _TicketsDialogState({
    this.productId,
    this.userTickets,
  });

  buildSlider() {
    return !_enabled ? CustomProgressIndicator() :  FluidSlider(
            sliderColor: Theme.of(context).accentColor,
            labelsTextStyle: TextStyle(
                fontSize: 16.0,
                color: Colors.white,
                fontWeight: FontWeight.w600),
            valueTextStyle:
                TextStyle(fontSize: 16.0, fontWeight: FontWeight.w600),
            value: _amount,
            onChanged: (double newValue) {
              setState(() {
                _amount = newValue;
              });
            },
            min: 1.0,
            max: userTickets.roundToDouble(),
          );
  }

  dialogContent(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: Consts.padding,
        bottom: Consts.padding,
        left: Consts.padding,
        right: Consts.padding,
      ),
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(Consts.padding),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min, // To make the card compact
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              "Choose ticket quantity",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          SizedBox(height: 24.0),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 4.0),
            child: Align(alignment: Alignment.center, child: buildSlider()),
          ),
          SizedBox(height: 24.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                onPressed: () {
                  Navigator.of(context).pop(); // To close the dialog
                },
                child: Text("CANCEL",
                    style: TextStyle(
                      color: Theme.of(context).accentColor,
                    )),
              ),
              FlatButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0)),
                onPressed: !_enabled ? null : () {
                  setState(() {
                    _enabled = false;
                  });
                  StoreProvider.of<AppState>(context).dispatch(SendBid(productId, _amount));
                  Navigator.of(context).pop(); // To close the dialog
                },
                child: Text("ADD",
                    style: TextStyle(
                      color: Theme.of(context).accentColor,
                    )),
              ),
            ],
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }
}

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 66.0;
}
