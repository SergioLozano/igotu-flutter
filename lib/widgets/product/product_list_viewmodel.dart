import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:igotu/models/product.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:redux/redux.dart';

// ignore: prefer_double_quotes
part 'product_list_viewmodel.g.dart';

abstract class ProductListViewModel
    implements Built<ProductListViewModel, ProductListViewModelBuilder> {
  BuiltList<Product> get products;

  ProductListViewModel._();

  factory ProductListViewModel(
          [void Function(ProductListViewModelBuilder) updates]) =
      _$ProductListViewModel;

  static ProductListViewModel fromStore(Store<AppState> store) {
    return ProductListViewModel(
        (p) => p..products = store.state.products.toBuilder());
  }

  //TODO: implement hasData
}
