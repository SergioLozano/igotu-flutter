import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/widgets/product/product_list_item.dart';
import 'package:igotu/widgets/product/product_list_viewmodel.dart';

class ProductList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, ProductListViewModel>(
        converter: ProductListViewModel.fromStore,
        distinct: true,
        builder: (context, vm) {
          List<GridTile> gridTiles = [];

          vm.products.forEach((product) {
            gridTiles.add(GridTile(
              child: ProductListItem(
                product: product,
              ),
            ));
          });

          return GridView.count(
              primary: false,
              crossAxisCount: 2,
              mainAxisSpacing: 10,
              crossAxisSpacing: 5,
              childAspectRatio: 1.9 / 3,
              controller: new ScrollController(keepScrollOffset: false),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              children: gridTiles);
        });
  }
}
