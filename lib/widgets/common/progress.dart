import 'package:flutter/material.dart';

class CustomProgressIndicator extends StatelessWidget {
  final bool isLinear;
  final Color color;

  CustomProgressIndicator({this.isLinear = false, this.color});

  @override
  Widget build(BuildContext context) {
    return isLinear ? linearProgress() : circularProgress();
  }

  Container circularProgress() {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.all(8.0),
      child: CircularProgressIndicator(
        valueColor:
            AlwaysStoppedAnimation(color != null ? color : Colors.deepPurple),
      ),
    );
  }

  Container linearProgress() {
    return Container(
      padding: EdgeInsets.only(bottom: 10.0),
      child: LinearProgressIndicator(
        valueColor:
            AlwaysStoppedAnimation(color != null ? color : Colors.deepPurple),
      ),
    );
  }
}
