// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'header_viewmodel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HeaderViewModel extends HeaderViewModel {
  @override
  final String username;
  @override
  final String image;
  @override
  final int lvl;
  @override
  final int nextLvlXp;
  @override
  final int userXp;
  @override
  final int tickets;
  @override
  final int goldenTickets;

  factory _$HeaderViewModel([void Function(HeaderViewModelBuilder) updates]) =>
      (new HeaderViewModelBuilder()..update(updates)).build();

  _$HeaderViewModel._(
      {this.username,
      this.image,
      this.lvl,
      this.nextLvlXp,
      this.userXp,
      this.tickets,
      this.goldenTickets})
      : super._() {
    if (username == null) {
      throw new BuiltValueNullFieldError('HeaderViewModel', 'username');
    }
    if (image == null) {
      throw new BuiltValueNullFieldError('HeaderViewModel', 'image');
    }
    if (lvl == null) {
      throw new BuiltValueNullFieldError('HeaderViewModel', 'lvl');
    }
    if (nextLvlXp == null) {
      throw new BuiltValueNullFieldError('HeaderViewModel', 'nextLvlXp');
    }
    if (userXp == null) {
      throw new BuiltValueNullFieldError('HeaderViewModel', 'userXp');
    }
    if (tickets == null) {
      throw new BuiltValueNullFieldError('HeaderViewModel', 'tickets');
    }
    if (goldenTickets == null) {
      throw new BuiltValueNullFieldError('HeaderViewModel', 'goldenTickets');
    }
  }

  @override
  HeaderViewModel rebuild(void Function(HeaderViewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HeaderViewModelBuilder toBuilder() =>
      new HeaderViewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HeaderViewModel &&
        username == other.username &&
        image == other.image &&
        lvl == other.lvl &&
        nextLvlXp == other.nextLvlXp &&
        userXp == other.userXp &&
        tickets == other.tickets &&
        goldenTickets == other.goldenTickets;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, username.hashCode), image.hashCode),
                        lvl.hashCode),
                    nextLvlXp.hashCode),
                userXp.hashCode),
            tickets.hashCode),
        goldenTickets.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('HeaderViewModel')
          ..add('username', username)
          ..add('image', image)
          ..add('lvl', lvl)
          ..add('nextLvlXp', nextLvlXp)
          ..add('userXp', userXp)
          ..add('tickets', tickets)
          ..add('goldenTickets', goldenTickets))
        .toString();
  }
}

class HeaderViewModelBuilder
    implements Builder<HeaderViewModel, HeaderViewModelBuilder> {
  _$HeaderViewModel _$v;

  String _username;
  String get username => _$this._username;
  set username(String username) => _$this._username = username;

  String _image;
  String get image => _$this._image;
  set image(String image) => _$this._image = image;

  int _lvl;
  int get lvl => _$this._lvl;
  set lvl(int lvl) => _$this._lvl = lvl;

  int _nextLvlXp;
  int get nextLvlXp => _$this._nextLvlXp;
  set nextLvlXp(int nextLvlXp) => _$this._nextLvlXp = nextLvlXp;

  int _userXp;
  int get userXp => _$this._userXp;
  set userXp(int userXp) => _$this._userXp = userXp;

  int _tickets;
  int get tickets => _$this._tickets;
  set tickets(int tickets) => _$this._tickets = tickets;

  int _goldenTickets;
  int get goldenTickets => _$this._goldenTickets;
  set goldenTickets(int goldenTickets) => _$this._goldenTickets = goldenTickets;

  HeaderViewModelBuilder();

  HeaderViewModelBuilder get _$this {
    if (_$v != null) {
      _username = _$v.username;
      _image = _$v.image;
      _lvl = _$v.lvl;
      _nextLvlXp = _$v.nextLvlXp;
      _userXp = _$v.userXp;
      _tickets = _$v.tickets;
      _goldenTickets = _$v.goldenTickets;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HeaderViewModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$HeaderViewModel;
  }

  @override
  void update(void Function(HeaderViewModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$HeaderViewModel build() {
    final _$result = _$v ??
        new _$HeaderViewModel._(
            username: username,
            image: image,
            lvl: lvl,
            nextLvlXp: nextLvlXp,
            userXp: userXp,
            tickets: tickets,
            goldenTickets: goldenTickets);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
