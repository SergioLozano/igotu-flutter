import 'dart:math';

import 'package:built_value/built_value.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:redux/redux.dart';

// ignore: prefer_double_quotes
part 'header_viewmodel.g.dart';

int levels = 99;
int xpForFirstLvl = 100;
double b = log(1 + 0.15);
double a = xpForFirstLvl / (exp(b) - 1.0);

abstract class HeaderViewModel
    implements Built<HeaderViewModel, HeaderViewModelBuilder> {
  String get username;

  String get image;

  int get lvl;

  int get nextLvlXp;

  int get userXp;

  int get tickets;

  int get goldenTickets;

  HeaderViewModel._();

  factory HeaderViewModel([void Function(HeaderViewModelBuilder) updates]) =
      _$HeaderViewModel;

  static HeaderViewModel fromStore(Store<AppState> store) {
    final user = store.state.user;

    final int currentLvl = _levelforExperience(user.exp);

    return HeaderViewModel((h) => h
      ..username = user.username
      ..image = user.image
      ..lvl = currentLvl
      ..nextLvlXp = _experienceForLevel(currentLvl + 1)
      ..userXp = user.exp
      ..tickets = user.tickets
      ..goldenTickets = user.goldenTickets
      );
  }

  static int _experienceForLevel(int lvl) {
    int oldXp = (a * exp(b * (lvl - 1))).round();
    int newXp = (a * exp(b * lvl)).round();

    return newXp - oldXp;
  }

  static int _levelforExperience(int xp) {
    for (int i = 1; i <= levels; i++) {
      int oldXp = (a * exp(b * (i - 1))).round();
      int newXp = (a * exp(b * i)).round();

      int currentXp = newXp - oldXp;

      if (currentXp >= xp) {
        return i;
      }
    }
    return levels;
  }
}
