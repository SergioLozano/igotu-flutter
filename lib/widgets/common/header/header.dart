import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/widgets/common/header/header_viewmodel.dart';

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, HeaderViewModel>(
      converter: HeaderViewModel.fromStore,
      distinct: true,
      builder: (context, vm) {
        return header(context, vm);
      },
    );
  }
}

Container buildProfile(HeaderViewModel vm) {
  final double sliderValue = vm.userXp / vm.nextLvlXp;
  return Container(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(right: 8.0),
                child: CircleAvatar(
                  radius: 32.0,
                  backgroundColor: Colors.transparent,
                  child: CachedNetworkImage(
                    imageUrl: vm.image,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(vm.username),
                        ),
                        LinearProgressIndicator(
                          value: 0.5,
                          backgroundColor: Colors.deepPurple[100],
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("Lv.${vm.lvl}"),
                        Text("${vm.userXp}/${vm.nextLvlXp}")
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ],
      ));
}

SafeArea header(BuildContext context, HeaderViewModel vm) {
  return SafeArea(
      child: Container(
    padding: EdgeInsets.symmetric(horizontal: 16.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          flex: 5,
          child: buildProfile(vm),
        ),
        Container(
          padding: EdgeInsets.only(left: 24.0),
          child: Row(
            children: <Widget>[
              SizedBox(
                width: 24.0,
                height: 24.0,
                child: SvgPicture.asset('assets/icons/golden_tickets.svg'),
              ),
              SizedBox(
                width: 4.0,
              ),
              Text(
                vm.goldenTickets.toString(),
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              ),
              SizedBox(
                width: 8.0,
              ),
              SizedBox(
                width: 24.0,
                height: 24.0,
                child: SvgPicture.asset('assets/icons/tickets.svg'),
              ),
              SizedBox(
                width: 4.0,
              ),
              Text(
                vm.tickets.toString(),
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              )
            ],
          ),
        )
      ],
    ),
  ));
}
