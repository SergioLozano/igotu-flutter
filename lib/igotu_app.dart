import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:igotu/data/bid_repository.dart';
import 'package:igotu/data/participant_repository.dart';
import 'package:igotu/data/product_repository.dart';
import 'package:igotu/data/user_repository.dart';
import 'package:igotu/igotu_localization.dart';
import 'package:igotu/pages/home/main_screen.dart';
import 'package:igotu/pages/login/login_screen.dart';
import 'package:igotu/pages/login/phone_login_screen.dart';
import 'package:igotu/pages/product/product_screen.dart';
import 'package:igotu/redux/app_middleware.dart';
import 'package:igotu/redux/app_reducer.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/authentication/auth_actions.dart';
import 'package:igotu/redux/authentication/auth_middleware.dart';
import 'package:igotu/redux/bid/bid_middleware.dart';
import 'package:igotu/redux/participants/participants_middleware.dart';
import 'package:igotu/redux/products/product_middleware.dart';
import 'package:igotu/redux/user/user_actions.dart';
import 'package:igotu/redux/user/user_middleware.dart';
import 'package:igotu/routes.dart';
import 'package:igotu/theme.dart';
import 'package:redux/redux.dart';

class IgotuApp extends StatefulWidget {
  const IgotuApp({
    Key key,
  }) : super(key: key);

  @override
  _IgotuAppState createState() => _IgotuAppState();
}

class _IgotuAppState extends State<IgotuApp> {
  Store<AppState> store;
  static final _navigatorKey = GlobalKey<NavigatorState>();
  //final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final userRepo = UserRepository(FirebaseAuth.instance, Firestore.instance);
  final productRepository = ProductRepository(Firestore.instance);
  final bidRepository = BidRespository(Firestore.instance);
  final participantRespository = ParticipantRespository(Firestore.instance);

  @override
  void initState() {
    super.initState();
    store = Store<AppState>(appReducer,
        initialState: AppState.init(),
        middleware: createStoreMiddleware()
          ..addAll(createAuthenticationMiddleware(userRepo, _navigatorKey))
          ..addAll(createUserMiddleware(userRepo))
          ..addAll(createProductsMiddleware(productRepository))
          ..addAll(createBidsMiddleware(bidRepository))
          ..addAll(createParticipantsMiddleware(participantRespository)));
    store.dispatch(VerifyAuthenticationState());
  }

  _updateUserLocale(context) {
    final localCode = IgotuLocalizations.of(context).locale.languageCode;
    StoreProvider.of<AppState>(context)
        .dispatch(UpdateUserLocaleAction(localCode));
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.deepPurple[900]));
    return StoreProvider(
      store: store,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        localizationsDelegates: localizationsDelegates,
        supportedLocales: [const Locale("en", "EN"), const Locale("es", "ES")],
        title: "igotU",
        navigatorKey: _navigatorKey,
        theme: AppTheme.theme,
        routes: {
          Routes.login: (context) {
            return LoginScreen();
          },
          Routes.phoneLogin: (context) {
            return PhoneLoginScreen();
          },
          Routes.home: (context) {
            _updateUserLocale(context);
            return MainScreen();
          },
          Routes.product: (context) {
            return ProductScreen();
          }
        },
      ),
    );
  }
}
