import "package:flutter/foundation.dart";
import "package:flutter/material.dart";
import "package:flutter_localizations/flutter_localizations.dart";

import "cupertinoLocalizationDelegate.dart";

final localizationsDelegates = <LocalizationsDelegate>[
  const IgotuLocalizationsDelegate(),
  GlobalMaterialLocalizations.delegate,
  GlobalWidgetsLocalizations.delegate,
  const FallbackCupertinoLocalisationsDelegate()
];

class IgotuLocalizations {
  IgotuLocalizations(this.locale);

  final Locale locale;

  static IgotuLocalizations of(BuildContext context) {
    return Localizations.of<IgotuLocalizations>(context, IgotuLocalizations);
  }

  static final Map<String, Map<String, String>> _localizedValues = {
    "en": {
      "log_out": "Log out",
      "log_in": "Log in",
      "hello_name": "Hello {name}!",
      "welcome": "Welcome to \n iGotU",
      "login_twitch": "Login with Twitch",
      "login_not_on_twitch": "Not on Twitch?",
      "login_fail_user_not_found": "Login failed. No such user exists.",
      "login_fail": "Login failed. Code: '{code}'",
      "login_terms_hint": "By siging up you agree to the",
      "login_terms": "Terms of Use Privacy Policy and Rules",
      "validate_phone_number": "Please enter valid mobile number",
      "validate_phone_number_empty": "Please enter your phone number",
      "generic_soon_alert_title": "We are working on it",
      "generic_soon_alert_message":
          "This functionality will be available soon.",
      "generic_yes": "Yes",
      "generic_cancel": "Cancel",
      "generic_next": "Next",
      "generic_ok": "OK",
      "generic_back": "Back",
      "generic_at": "at",
      "generic_you": "You",
      "generic_save": "Save",
      "generic_edit": "Edit",
      "generic_delete": "Delete",
      "generic_invite": "Invite",
      "generic_create": "Create",
      "generic_verify": "Verify",
      "generic_error" : "Seems, there is an error",
      "generic_search_no_results": "Your search found no results",
      "platform_alert_access_title": "Please grant iGotU access",
      "platform_alert_access_body":
          "For this feature to work the app need access to your {RESOURCE}",
      "platform_alert_access_resource_camera": "camera",
      "platform_alert_access_resource_photos": "photos",
      "user_edit_name_label": "Your name",
      "user_edit_name_helper": "Maximum 30 characters",
      "user_edit_name_error": "Name cannot be empty",
      "user_edit_status_label": "Your status",
      "user_edit_status_helper": "Maximum 200 characters",
      "user_deleted": "[deleted]",
      "user_country_select": "Select your country",
      "user_enter_phone": "Enter your phone",
      "user_phone_login_hint": "By continuing you may receive an SMS for verification. Message and data rates may apply.",
      "action_send_code": "Send code",
      "action_verify_code": "Verify code",
      "bubble_empty_participants": "Pts! No one is participating... \n Good chance to get this prize!",
      "title_login_signup": "Login or sign up",
      "title_prize_detail": "Prize Detail",
    },
    "es": {
      "log_out": "Cerrar sesión",
      "log_in": "Iniciar sesión",
      "hello_name": "¡Hola {name}!",
      "welcome": "Bienvenido a \n iGotU",
      "login_twitch": "Inicia sesión con Twitch",
      "login_not_on_twitch": "¿Sin cuenta en Twitch?",
      "login_fail_user_not_found":
          "El usuario introducido no se encuentra registrado.",
      "login_fail": "Error en el inicio de sesión. Código de error: '{code}'",
      "login_terms_hint": "Al registrarte aceptas los siguientes",
      "login_terms": "Términos de uso Política de Privacidad y Reglas",
      "validate_phone_number": "Porfavor introduce un número válido",
      "validate_phone_number_empty": "Porfavor introduce tu número de telefono",
      "generic_soon_alert_title": "Estamos trabajando en ello.",
      "generic_soon_alert_message":
          "Esta funcionalidad estará disponible pronto.",
      "generic_yes": "Sí",
      "generic_cancel": "Cancelar",
      "generic_next": "Siguiente",
      "generic_ok": "Aceptar",
      "generic_back": "Atrás",
      "generic_at": "en",
      "generic_you": "Tú",
      "generic_save": "Guardar",
      "generic_edit": "Editar",
      "generic_delete": "Eliminar",
      "generic_invite": "Invitar",
      "generic_create": "Crear",
      "generic_verify": "Verificar",
      "generic_error" : "Parece que ha ocurrido un error inesperado.",
      "generic_search_no_results": "Sin resultados",
      "platform_alert_access_title": "Porfavor, concede ascceso a iGotU",
      "platform_alert_access_body":
          "For this feature to work the app need access to your {RESOURCE}",
      "platform_alert_access_resource_camera": "cámara",
      "platform_alert_access_resource_photos": "galería",
      "user_edit_name_label": "Tu nombre",
      "user_edit_name_helper": "Máximo 30 carácteres",
      "user_edit_name_error": "Introduce un nombre.",
      "user_edit_status_label": "Tu estado",
      "user_edit_status_helper": "Máximo 200 carácteres",
      "user_deleted": "[deleted]",
      "user_country_select": "Selecciona tu país",
      "user_enter_phone": "Introduce tu número",
      "user_phone_login_hint": "Te enviaremos un mensaje de texto con el código de verificación. Es posible que se apliquen las tarifas estandar por mensajes y datos.",
      "action_send_code": "Enviar código",
      "action_verify_code": "Verificar código",
      "bubble_empty_participants": "Pts! Ahora mismo no hay nadie participando. ¡Con unos pocos tickets te puedes hacer con el premio!",
      "title_login_signup": "Inicia sesión o registrate",
      "title_prize_detail": "Información del premio",
    }
  };

  /// This method returns the localized value of the passed id
  /// it defaults to english if the locale is missing
  String _localizedValue(String id) =>
      _localizedValues[locale.languageCode][id] ?? _localizedValues["en"][id];

  // Generic
  String get genericSoonAlertTitle {
    return _localizedValue("generic_soon_alert_title");
  }

  String get genericSoonAlertMessage {
    return _localizedValue("generic_soon_alert_message");
  }

  String get yes {
    return _localizedValue("generic_yes");
  }

  String get cancel {
    return _localizedValue("generic_cancel");
  }

  String get next {
    return _localizedValue("generic_next");
  }

  String get ok {
    return _localizedValue("generic_ok");
  }

  String get back {
    return _localizedValue("generic_back");
  }

  String get at => _localizedValue("generic_at");

  String get you => _localizedValue("generic_you");

  String get save => _localizedValue("generic_save");

  String get edit => _localizedValue("generic_edit");

  String get delete => _localizedValue("generic_delete");

  String get error => _localizedValue("generic_error");

  String get searchNoResult => _localizedValue("generic_search_no_results");

  String get create => _localizedValue("generic_create");

  String get verify => _localizedValue("generic_verify");

  String get sendDirectMessage => _localizedValue("user_send_direct_message");

  String get userEditNameLabel => _localizedValue("user_edit_name_label");

  String get userEditNameHelper => _localizedValue("user_edit_name_helper");

  String get userEditNameError => _localizedValue("user_edit_name_error");

  String get userEditStatusLabel => _localizedValue("user_edit_status_label");

  String get userEditStatusHelper => _localizedValue("user_edit_status_helper");

  String get deletedUser => _localizedValue("user_deleted");

  String get userCountrySelect => _localizedValue("user_country_select");

  String get userEnterPhone => _localizedValue("user_enter_phone");

  ///Validators
  String get emptyPhone => _localizedValue("validate_phone_number_empty");

  String get validPhone => _localizedValue("validate_phone_number");

  ///Actions
  String get actionSendCode => _localizedValue("action_send_code");
  String get actionVerifyCode => _localizedValue("action_verify_code");

  /// Auth
  String get logIn {
    return _localizedValue("log_in");
  }

  String get logOut {
    return _localizedValue("log_out");
  }

  String get twitchLogin => _localizedValue("login_twitch");

  String get notTwitchLogin => _localizedValue("login_not_on_twitch");

  String get termsHint => _localizedValue("login_terms_hint");

  String get terms => _localizedValue("login_terms");

  String hello(String name) {
    return _localizedValue("hello_name").replaceAll("{name}", name);
  }

  String get welcome => _localizedValue("welcome");

  String get phoneVerificationHint => _localizedValue("user_phone_login_hint");

  String authErrorMessage(String code) {
    switch (code) {
      case "ERROR_USER_NOT_FOUND":
        return _localizedValue("login_fail_user_not_found");
      default:
        return _localizedValue("login_fail").replaceAll("{code}", code);
    }
  }

  /// Platform
  String get platformAlertAccessTitle =>
      _localizedValue("platform_alert_access_title");

  String get platformAlertAccessResourceCamera =>
      _localizedValue("platform_alert_access_resource_camera");
  String get platformAlertAccessResourcePhotos =>
      _localizedValue("platform_alert_access_resource_photos");

  
  /// Bubbles
  String get emptyParticipantList => _localizedValue("bubble_empty_participants");

  /// Screen titles
  String get loginOrSignupTitle => _localizedValue("title_login_signup");
  String get prizeDetailTitle => _localizedValue("title_prize_detail");
}

class IgotuLocalizationsDelegate
    extends LocalizationsDelegate<IgotuLocalizations> {
  const IgotuLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => ["en", "es"].contains(locale.languageCode);

  @override
  Future<IgotuLocalizations> load(Locale locale) {
    return SynchronousFuture<IgotuLocalizations>(IgotuLocalizations(locale));
  }

  @override
  bool shouldReload(LocalizationsDelegate<IgotuLocalizations> old) => false;
}
