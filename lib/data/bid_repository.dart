import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:igotu/data/firestore_paths.dart';
import 'package:igotu/models/bid.dart';

class BidRespository {
  static const String PRODUCTID = "productId";
  static const String AMOUNT = "amount";
  static const String TIMESTAMP = "timestamp";

  final Firestore _firestore;

  BidRespository(this._firestore);

  Future<Bid> getBid(String userId, String bidId) async {
    final document =
        await _firestore.document(FirestorePaths.bidPath(userId, bidId)).get();
    return fromDoc(document);
  }

  Stream<List<Bid>> getBidsStream(String userId) {
    return _firestore
        .collection(FirestorePaths.bidsPath(userId))
        .orderBy('timestamp', descending: true)
        .limit(25)
        .snapshots()
        .map((querySnapshot) {
      return querySnapshot.documents.map((doc) => fromDoc(doc)).toList();
    });
  }

  Future<Bid> sendBid(String userId, Bid bid) async {
    final bidsPath = FirestorePaths.bidsPath(userId);
    final data = toMap(bid);
    final reference = await _firestore.collection(bidsPath).add(data);
    final doc = await reference.get();
    return fromDoc(doc);
  }

  static Bid fromDoc(DocumentSnapshot document) {
    return Bid((b) => b
      ..id = document.documentID
      ..productId = document[PRODUCTID]
      ..amount = document[AMOUNT]
      ..timestamp = DateTime.fromMillisecondsSinceEpoch(
          int.tryParse(document[TIMESTAMP]) ?? 0));
  }

  static toMap(Bid bid) {
    return {
      PRODUCTID: bid.productId,
      AMOUNT: bid.amount.truncateToDouble(),
      TIMESTAMP: bid.timestamp.millisecondsSinceEpoch.toString(),
    };
  }
}
