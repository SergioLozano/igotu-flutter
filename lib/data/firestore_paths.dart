class FirestorePaths {
  static const PATH_USERS = "users";
  static const PATH_PRODUCTS = "products";
  static const PATH_BIDS = "bids";
  static const PATH_USER_BIDS = "userBids";
  static const PATH_PARTICIPANTS = "participants";

  static String userPath(String userId) {
    return "$PATH_USERS/$userId";
  }

  static String productPath(String productId) {
    return "$PATH_PRODUCTS/$productId";
  }

  static String bidPath(String userId, String bidId) {
    return "$PATH_BIDS/$userId/$PATH_USER_BIDS/$bidId";
  }

  static String bidsPath(String userId) {
    return "$PATH_BIDS/$userId/$PATH_USER_BIDS";
  }

  static String participantsPath(String productId) {
    return "$PATH_PRODUCTS/$productId/$PATH_PARTICIPANTS";
  }
}