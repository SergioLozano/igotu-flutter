import 'package:built_collection/built_collection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:igotu/data/firestore_paths.dart';
import 'package:igotu/models/product.dart';

class ProductRepository {
  static const String NAME = "name";
  static const String DESCRIPTION = "description";
  static const String COVERURL = "coverUrl";
  static const String IMAGES = "images";
  static const String QUANTITY = "quantity";
  static const String TICKETS = "tickets";
  static const String TIMESTAMP = "timestamp";

  final Firestore _firestore;

  ProductRepository(this._firestore);

  Future<Product> getProduct(String productId) async {
    final document =
        await _firestore.document(FirestorePaths.productPath(productId)).get();
    return fromDoc(document);
  }

  Stream<List<Product>> getProductsStream() {
    return _firestore
        .collection(FirestorePaths.PATH_PRODUCTS)
        .orderBy('timestamp', descending: true)
        .limit(6)
        .snapshots()
        .map((querySnapshot) {
      return querySnapshot.documents.map((doc) => fromDoc(doc)).toList();
    });
  }

  static Product fromDoc(DocumentSnapshot document) {
    return Product((p) => p
      ..id = document.documentID
      ..name = document[NAME]
      ..description = document[DESCRIPTION]
      ..coverUrl = document[COVERURL]
      ..images = ListBuilder(document[IMAGES] ?? [])
      ..quantity = document[QUANTITY]
      ..tickets = _parseTickets(document)
      ..timestamp = document[TIMESTAMP].toDate());
  }

  static int _parseTickets(doc) {
    dynamic tickets = doc[TICKETS];

    if (tickets == null) {
      return 0;
    }
    int count = 0;
    tickets.values.forEach((val) {
      if (val > 0) {
        count += val;
      }
    });
    return count;
  }
}
