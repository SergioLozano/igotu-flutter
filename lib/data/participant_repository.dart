import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:igotu/data/firestore_paths.dart';
import 'package:igotu/models/participant.dart';

class ParticipantRespository {
  static const USERNAME = "username";
  static const USERIMAGE = "image";
  static const TICKETS = "tickets";

  final Firestore _firestore;

  ParticipantRespository(this._firestore);

  Stream<List<Participant>> getParticipantsStream(String productId) {
    return _firestore
        .collection(FirestorePaths.participantsPath(productId))
        .orderBy(TICKETS, descending: false)
        .limit(50)
        .snapshots(includeMetadataChanges: true)
        .map((querySnapshot) {
      return querySnapshot.documents.map((doc) => fromDoc(doc)).toList();
    });
  }

  static Participant fromDoc(DocumentSnapshot document) {
    return Participant((p) => p
      ..uid = document.documentID
      ..username = document[USERNAME]
      ..image = document[USERIMAGE]
      ..tickets = document[TICKETS]);
  }
}
