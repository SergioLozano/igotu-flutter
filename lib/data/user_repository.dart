import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_web_auth/flutter_web_auth.dart';
import 'package:igotu/constants/constants.dart';
import 'package:igotu/models/user.dart';
import 'firestore_paths.dart';

import 'package:http/http.dart' as http;

class UserRepository {
  static const USERNAME = "username";
  static const EMAIL = "email";
  static const PHONE = "phone";
  static const IMAGE = "image";
  static const TICKETS = "tickets";
  static const GOLDEN_TICKETS = "goldenTickets";
  static const EXP = "exp";
  static const UID = "uid";
  static const TOKEN = "token";
  static const LOCALE = "locale";

  final FirebaseAuth _firebaseAuth;
  final Firestore _firestore;

  const UserRepository(
    this._firebaseAuth,
    this._firestore,
  );

  Stream<User> getUserStream(userId) {
    return _firestore
        .collection(FirestorePaths.PATH_USERS)
        .document(userId)
        .snapshots()
        .map((userSnapshot) {
      return fromDoc(userSnapshot);
    });
  }

  Stream<User> getAuthenticationStateChange() {
    return _firebaseAuth.onAuthStateChanged.asyncMap((firebaseUser) {
      return _fromFirebaseUser(firebaseUser);
    });
  }

  Future<String> twitchLogIn() async {
    final authUrl = await authorizeUrl();

    final result = await FlutterWebAuth.authenticate(
        url: authUrl, callbackUrlScheme: Constants.APP_CALLBACK);

    String code = Uri.parse(result).queryParameters['code'];
    String state = Uri.parse(result).queryParameters['state'];

    var queryParameters = {'state': state, 'code': code};

    Uri uri =
        Uri.http(Constants.SERVER_URL, Constants.TOKEN_URL, queryParameters);

    final tokenResponse = await http.get(uri, headers: {"cookies": state});

    return tokenResponse.body;
  }

  Future<String> authorizeUrl() async {
    final urlResponse = await http.get(Constants.AUTHORIZE_URL);

    return urlResponse.body;
  }

  Future<User> signInWithCredential(AuthCredential credential) async {
    final firebaseUser = await _firebaseAuth.signInWithCredential(credential);

    return await _fromFirebaseUser(firebaseUser.user);
  }

  Future<User> signInWithCustomToken(String token) async {
    final firebaseUser =
        await _firebaseAuth.signInWithCustomToken(token: token);

    return await _fromFirebaseUser(firebaseUser.user);
  }

  Future<User> _fromFirebaseUser(FirebaseUser firebaseUser) async {
    if (firebaseUser == null) return Future.value(null);

    final documentReference =
        _firestore.document(FirestorePaths.userPath(firebaseUser.uid));
    final snapshot = await documentReference.get();

    User user;
    if (snapshot.data == null) {
      user = User((u) => u
            ..uid = firebaseUser.uid
            ..email = firebaseUser.email
            ..username = firebaseUser
                .email // Default name will be the email, let user change later
          );
      await documentReference.setData(toMap(user));
    } else {
      user = fromDoc(snapshot);
    }
    return user;
  }

  Future<void> verifyPhoneNumber(
      String phone,
      PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout,
      PhoneCodeSent codeSent,
      PhoneVerificationCompleted verificationCompleted,
      PhoneVerificationFailed verificationFailed) {
    return _firebaseAuth.verifyPhoneNumber(
        phoneNumber: phone,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout,
        codeSent: codeSent,
        timeout: Duration(seconds: 90),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed);
  }

  Future<void> logOut() async {
    await updateUserToken(null);
    await _firebaseAuth.signOut();
  }

  Future<void> updateUserToken(String token) async {
    final firebaseUser = await _firebaseAuth.currentUser();
    if (firebaseUser != null) {
      final documentReference =
          _firestore.document(FirestorePaths.userPath(firebaseUser.uid));
      return documentReference.updateData({TOKEN: token});
    }
  }

  ///
  /// Allows to update the User, but only the following fields:
  /// - name
  /// - status
  /// - image
  ///

  Future<void> updateUser(User user) async {
    final firebaseUser = await _firebaseAuth.currentUser();
    if (firebaseUser != null) {
      final documentReference =
          _firestore.document(FirestorePaths.userPath(firebaseUser.uid));
      return documentReference
          .updateData({USERNAME: user.username, IMAGE: user.image});
    }
  }

  // Sets a users locale on our backend.
  // The locale is used to send localized notifications.
  Future<void> updateUserLocale(String locale) async {
    _firebaseAuth.setLanguageCode(locale);
    final firebaseUser = await _firebaseAuth.currentUser();
    if (firebaseUser != null) {
      final documentReference =
          _firestore.document(FirestorePaths.userPath(firebaseUser.uid));
      return documentReference.updateData({
        LOCALE: locale,
      });
    }
  }

  static toMap(User user) {
    return {UID: user.uid, USERNAME: user.username, EMAIL: user.email};
  }

  static User fromDoc(DocumentSnapshot document) {
    return User((u) => u
      ..uid = document.documentID
      ..email = document[EMAIL]
      ..phone = document[PHONE]
      ..username = document[USERNAME]
      ..image = document[IMAGE]      
      ..tickets = document[TICKETS] ?? 0
      ..goldenTickets = document[GOLDEN_TICKETS] ?? 0
      ..exp = document[EXP] ?? 0
      );
  }
}
