/// Actions are payloads of information that send data from your application to
/// your store. They are the only source of information for the store.
///
/// They are PODOs (Plain Old Dart Objects).
///
class ConnectToDataSource {
  @override
  String toString() {
    return "ConnectToDataSource{}";
  }
}