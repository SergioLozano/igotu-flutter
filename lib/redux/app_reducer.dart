import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/authentication/auth_reducer.dart';
import 'package:igotu/redux/bid/bid_reducer.dart';
import 'package:igotu/redux/participants/participants_reducer.dart';
import 'package:igotu/redux/products/product_reducer.dart';
import 'package:igotu/redux/user/user_reducer.dart';
import 'package:redux/redux.dart';

final appReducer = combineReducers<AppState>([
  ...authReducers,
  ...userReducers,
  ...productReducers,
  ...bidReducers,
  ...participantsReducer,
]);