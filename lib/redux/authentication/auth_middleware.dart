import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:igotu/data/user_repository.dart';
import 'package:igotu/models/phone_auth.dart';
import 'package:igotu/redux/app_actions.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/authentication/auth_actions.dart';
import 'package:igotu/redux/stream_subscriptions.dart';
import 'package:igotu/utils/logger.dart';
import 'package:igotu/routes.dart';
import 'package:redux/redux.dart';

/// Authentication Middleware
/// LogIn: Logging user in
/// LogOut: Logging user out
/// VerifyAuthenticationState: Verify if user is logged in

List<Middleware<AppState>> createAuthenticationMiddleware(
  UserRepository userRepository,
  GlobalKey<NavigatorState> navigatorKey,
) {
  return [
    TypedMiddleware<AppState, VerifyAuthenticationState>(
        _verifyAuthState(userRepository, navigatorKey)),
    TypedMiddleware<AppState, TwitchLogIn>(
        _twitchLogIn(userRepository, navigatorKey)),
    TypedMiddleware<AppState, PhoneLogIn>(
        _phoneVerification(userRepository, navigatorKey)),
    TypedMiddleware<AppState, VerifyPhoneCode>(
        _phoneCodeVerification(userRepository, navigatorKey)),
    TypedMiddleware<AppState, LogOutAction>(
        _authLogout(userRepository, navigatorKey)),
  ];
}

void Function(
  Store<AppState> store,
  VerifyAuthenticationState action,
  NextDispatcher next,
) _verifyAuthState(
  UserRepository userRepository,
  GlobalKey<NavigatorState> navigatorKey,
) {
  return (store, action, next) {
    next(action);
    userRepository.getAuthenticationStateChange().listen((user) {
      if (user == null) {
        navigatorKey.currentState.pushReplacementNamed(Routes.login);
      } else {
        store.dispatch(OnAuthenticated(user: user));
        store.dispatch(ConnectToDataSource());
      }
    });
  };
}

void Function(
  Store<AppState> store,
  dynamic action,
  NextDispatcher next,
) _authLogout(
  UserRepository userRepository,
  GlobalKey<NavigatorState> navigatorKey,
) {
  return (store, action, next) async {
    next(action);
    try {
      await userRepository.logOut();
      cancelAllSubscriptions();
      store.dispatch(OnLogoutSuccess());
    } catch (e) {
      Logger.w("Failed logout", e: e);
      store.dispatch(OnLogoutFail(e));
    }
  };
}

void Function(
  Store<AppState> store,
  dynamic action,
  NextDispatcher next,
) _twitchLogIn(
  UserRepository userRepository,
  GlobalKey<NavigatorState> navigatorKey,
) {
  return (store, action, next) async {
    next(action);
    try {
      final token = await userRepository.twitchLogIn();

      Logger.d("Token: $token");

      final user = await userRepository.signInWithCustomToken(token);

      if (user != null) {
        store.dispatch(OnAuthenticated(user: user));

        await navigatorKey.currentState.pushReplacementNamed(Routes.home);
        action.completer.complete();
      }
    } catch (e) {
      Logger.w("Failed code verification", e: e);
      action.completer.completeError(e);
    }
  };
}

void Function(
  Store<AppState> store,
  dynamic action,
  NextDispatcher next,
) _phoneCodeVerification(
  UserRepository userRepository,
  GlobalKey<NavigatorState> navigatorKey,
) {
  return (store, action, next) async {
    next(action);
    try {
      final loadingAuth = PhoneAuth((p) => p..authStatus = AuthStatus.LOADING);
      store.dispatch(PhoneAuthStatus(loadingAuth));

      AuthCredential credential = PhoneAuthProvider.getCredential(
          verificationId: store.state.phoneAuth.verificationId,
          smsCode: action.smsCode);

      final user = await userRepository.signInWithCredential(credential);

      if (user != null) {
        store.dispatch(OnAuthenticated(user: user));

        await navigatorKey.currentState.pushReplacementNamed(Routes.home);
        action.completer.complete();
      }
    } catch (e) {
      Logger.w("Failed code verification", e: e);
      final smsAuth = PhoneAuth((p) => p..authStatus = AuthStatus.SMS);
      store.dispatch(PhoneAuthStatus(smsAuth));
      action.completer.completeError(e);
    }
  };
}

void Function(
  Store<AppState> store,
  dynamic action,
  NextDispatcher next,
) _phoneVerification(
    UserRepository userRepository, GlobalKey<NavigatorState> navigatorKey) {
  return (store, action, next) async {
    next(action);

    try {
      PhoneVerificationFailed verificationFailed =
          (AuthException authException) {
        final phoneAuth = PhoneAuth((p) => p
          ..authStatus = AuthStatus.PHONE
          ..error = authException.message);
        store.dispatch(PhoneAuthStatus(phoneAuth));
        //TODO: show error to user.
        Logger.w(
            'Phone number verification failed. Error: ${authException.code}',
            e: authException,
            s: StackTrace.current);
        action.completer.completeError(authException);
      };

      PhoneVerificationCompleted verificationCompleted =
          (AuthCredential phoneAuthCredential) async {
        final loadingAuth =
            PhoneAuth((p) => p..authStatus = AuthStatus.LOADING);
        store.dispatch(PhoneAuthStatus(loadingAuth));

        print('Received phone auth credential: $phoneAuthCredential');

        final user =
            await userRepository.signInWithCredential(phoneAuthCredential);
        store.dispatch(OnAuthenticated(user: user));

        if (user != null) {
          await navigatorKey.currentState.pushReplacementNamed(Routes.home);
          action.completer.complete();
        } else {
          final smsAuth = PhoneAuth((p) => p..authStatus = AuthStatus.SMS);
          store.dispatch(PhoneAuthStatus(smsAuth));
        }
      };

      PhoneCodeSent codeSent =
          (String verificationId, [int forceResendingToken]) async {
        final loadingAuth =
            PhoneAuth((p) => p..authStatus = AuthStatus.LOADING);
        store.dispatch(PhoneAuthStatus(loadingAuth));

        final smsAuth = PhoneAuth((p) => p
          ..authStatus = AuthStatus.SMS
          ..verificationId = verificationId);
        store.dispatch(PhoneAuthStatus(smsAuth));

        print(
            'Please check your phone for the verification code. $verificationId');
      };

      PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
          (String verificationId) {
        final smsAuth = PhoneAuth((p) => p
          ..authStatus = AuthStatus.SMS
          ..verificationId = verificationId);
        store.dispatch(PhoneAuthStatus(smsAuth));
        print("auto retrieval timeout");
      };

      userRepository.verifyPhoneNumber(
          action.phoneNumber,
          codeAutoRetrievalTimeout,
          codeSent,
          verificationCompleted,
          verificationFailed);
    } on PlatformException catch (e) {
      Logger.w("Failed login", e: e);
      action.completer.completeError(e);
    }
  };
}
