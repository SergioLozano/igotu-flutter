import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/authentication/auth_actions.dart';
import 'package:redux/redux.dart';

final authReducers = <AppState Function(AppState, dynamic)>[
  TypedReducer<AppState, OnAuthenticated>(_onAuthenticated),
  TypedReducer<AppState, PhoneAuthStatus>(_onPhoneStatusChange),
  TypedReducer<AppState, OnLogoutSuccess>(_onLogout),
];

AppState _onAuthenticated(AppState state, OnAuthenticated action) {
  return state.rebuild((a) => a..user = action.user.toBuilder());
}

AppState _onPhoneStatusChange(AppState state, PhoneAuthStatus action) {
  return state.rebuild((a) => a..phoneAuth = action.authStatus.toBuilder());
}

AppState _onLogout(AppState state, OnLogoutSuccess action){
  return state.clear();
}
