import 'dart:async';

import 'package:igotu/models/phone_auth.dart';
import 'package:igotu/models/user.dart';
import 'package:meta/meta.dart';

// Authentication
class VerifyAuthenticationState {}

class TwitchLogIn {
  final Completer completer;

  TwitchLogIn({Completer completer}) : completer = completer ?? Completer();
}

class PhoneLogIn {
  final String phoneNumber;
  final Completer completer;

  PhoneLogIn({this.phoneNumber, Completer completer})
      : completer = completer ?? Completer();
}

class PhoneAuthStatus {
  final PhoneAuth authStatus;

  PhoneAuthStatus(this.authStatus);

  @override
  String toString() {
    return "PhoneAuthStatus{status: $authStatus}";
  }
}

class VerifyPhoneCode {
  final String smsCode;
  final Completer completer;

  VerifyPhoneCode({this.smsCode, Completer completer})
      : completer = completer ?? Completer();
}

@immutable
class OnAuthenticated {
  final User user;

  const OnAuthenticated({@required this.user});

  @override
  String toString() {
    return "OnAuthenticated{user: $user}";
  }
}

class LogOutAction {}

class OnLogoutSuccess {
  OnLogoutSuccess();

  @override
  String toString() {
    return "LogOut{user: null}";
  }
}

class OnLogoutFail {
  final dynamic error;

  OnLogoutFail(this.error);

  @override
  String toString() {
    return "OnLogoutFail{There was an error logging in: $error}";
  }
}
