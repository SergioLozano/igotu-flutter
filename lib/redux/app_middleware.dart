import 'package:igotu/redux/app_state.dart';
import 'package:igotu/utils/logger.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> createStoreMiddleware() {
  return [
    LoggerMiddleWare(),
  ];
}
