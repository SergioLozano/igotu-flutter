import 'dart:async';

import 'package:igotu/models/bid.dart';
import 'package:igotu/models/participant.dart';
import 'package:igotu/models/product.dart';
import 'package:igotu/models/user.dart';

// App user
StreamSubscription<User> userUpdateSubscription;
// List of products
StreamSubscription<List<Product>> productsSubscription;
// List of bids
StreamSubscription<List<Bid>> bidsSubscription;
// List of participants in product pool
StreamSubscription<List<Participant>> participantsSubscription;

/// Cancels all active subscriptions
///
/// Called on successful logout.
cancelAllSubscriptions() {
  userUpdateSubscription?.cancel();
  productsSubscription?.cancel();
  bidsSubscription?.cancel();
  participantsSubscription?.cancel();
}