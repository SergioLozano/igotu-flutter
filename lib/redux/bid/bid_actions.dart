import 'package:igotu/models/bid.dart';
import 'package:meta/meta.dart';

@immutable
class SendBid {
  final String productId;
  final double amount;

  const SendBid(this.productId, this.amount);

  @override
  String toString() {
    return "SendBid{product: $productId amount: $amount}";
  }
}

@immutable
class UpdateAllBids {
  final List<Bid> data;

  const UpdateAllBids(this.data);
}
