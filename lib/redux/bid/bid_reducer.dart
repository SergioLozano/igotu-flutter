import 'package:built_collection/built_collection.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/bid/bid_actions.dart';
import 'package:redux/redux.dart';

final bidReducers = <AppState Function(AppState, dynamic)>[
  TypedReducer<AppState, UpdateAllBids>(_onBidUpdated),
];

AppState _onBidUpdated(AppState state, UpdateAllBids action) {
  return state.rebuild((a) => a..bids = ListBuilder(action.data));
}