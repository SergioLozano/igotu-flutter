import 'package:igotu/data/bid_repository.dart';
import 'package:igotu/models/bid.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/bid/bid_actions.dart';
import 'package:igotu/utils/logger.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> createBidsMiddleware(
  BidRespository bidRespository,
) {
  return [
    TypedMiddleware<AppState, SendBid>(_sendBid(bidRespository)),
  ];
}

void Function(
  Store<AppState> store,
  SendBid action,
  NextDispatcher next,
) _sendBid(
  BidRespository bidRespository,
) {
  return (store, action, next) async {
    next(action);
    final userId = store.state.user.uid;
    final bid = Bid((b) => b
      ..productId = action.productId
      ..amount = action.amount);

    try {
      await bidRespository.sendBid(userId, bid);
    } catch (e) {
      Logger.e("Failed to send bid", e: e, s: StackTrace.current);
    }
  };
}
