import 'package:igotu/models/participant.dart';
import "package:meta/meta.dart";

@immutable
class LoadParticipants {
  final List<Participant> participants;

  const LoadParticipants({@required this.participants});

  @override
  String toString() {
    return "ParticipantsUpdatedAction{participants: $participants}";
  }
}