import 'package:built_collection/built_collection.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/participants/participants_actions.dart';
import 'package:redux/redux.dart';

final participantsReducer = <AppState Function(AppState, dynamic)>[
  TypedReducer<AppState, LoadParticipants>(_onParticipantsLoaded),
];

AppState _onParticipantsLoaded(AppState state, LoadParticipants action) {
  return state.rebuild((a) => a..participants = ListBuilder(action.participants));
}