import 'package:igotu/data/participant_repository.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/participants/participants_actions.dart';
import 'package:igotu/redux/products/product_actions.dart';
import 'package:igotu/redux/stream_subscriptions.dart';
import 'package:igotu/utils/logger.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> createParticipantsMiddleware(
  ParticipantRespository participantRespository,
) {
  return [
    TypedMiddleware<AppState, SelectProductByIdAction>(
        _listenParticipants(participantRespository)),
  ];
}

void Function(
  Store<AppState> store,
  SelectProductByIdAction action,
  NextDispatcher next,
) _listenParticipants(
  ParticipantRespository participantRespository,
) {
  return (store, action, next) {
    next(action);
    try {
      participantsSubscription?.cancel();
      participantsSubscription = participantRespository
          .getParticipantsStream(action.productId)
          .listen((participants) {
        store.dispatch(LoadParticipants(participants: participants));
      });
    } catch (e) {
      Logger.e("Failed to listen to participants in ${action.productId}",
          e: e, s: StackTrace.current);
    }
  };
}
