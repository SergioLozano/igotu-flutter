import 'package:igotu/data/user_repository.dart';
import 'package:igotu/redux/authentication/auth_actions.dart';
import 'package:igotu/redux/stream_subscriptions.dart';
import 'package:igotu/redux/user/user_actions.dart';
import 'package:igotu/utils/logger.dart';
import 'package:redux/redux.dart';
import 'package:igotu/redux/app_state.dart';

List<Middleware<AppState>> createUserMiddleware(
  UserRepository userRepository,
) {
  return [
    TypedMiddleware<AppState, OnAuthenticated>(_listenToUser(userRepository)),
    TypedMiddleware<AppState, UpdateUserLocaleAction>(
        _updateUserLocale(userRepository)),
    TypedMiddleware<AppState, UpdateUserAction>(_updateUser(userRepository)),
  ];
}

// Updates locale for logged in user.
void Function(
  Store<AppState> store,
  UpdateUserLocaleAction action,
  NextDispatcher next,
) _updateUserLocale(
  UserRepository userRepository,
) {
  return (store, action, next) async {
    next(action);

    try {
      // Updates user locale after login.
      await userRepository.updateUserLocale(action.locale);
    } catch (e) {
      Logger.e("Failed to update locale", e: e, s: StackTrace.current);
    }
  };
}

// Receives updates for the logged in user.
void Function(
  Store<AppState> store,
  OnAuthenticated action,
  NextDispatcher next,
) _listenToUser(
  UserRepository userRepository,
) {
  return (store, action, next) {
    next(action);
    try {
      userUpdateSubscription?.cancel();
      userUpdateSubscription =
          userRepository.getUserStream(action.user.uid).listen((user) {
        store.dispatch(OnUserUpdateAction(user));
      });
    } catch (e) {
      Logger.e("Failed to listen user", e: e, s: StackTrace.current);
    }
  };
}

void Function(
  Store<AppState> store,
  UpdateUserAction action,
  NextDispatcher next,
) _updateUser(
  UserRepository userRepository,
) {
  return (store, action, next) async {
    next(action);
    if (store.state.user.uid != action.user.uid) {
      action.completer
          .completeError(Exception("You can't update other users!"));
      return;
    }
    try {
      await userRepository.updateUser(action.user);
      store.dispatch(OnUserUpdateAction(action.user));
      action.completer.complete(action.user);
    } catch (error) {
      Logger.e("Failed to update user", e: error, s: StackTrace.current);
      action.completer.completeError(error);
    }
  };
}

