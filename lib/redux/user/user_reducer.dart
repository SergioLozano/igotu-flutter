import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/user/user_actions.dart';
import 'package:redux/redux.dart';

final userReducers = <AppState Function(AppState, dynamic)>[
  TypedReducer<AppState, OnUserUpdateAction>(_onUserUpdate),
];

AppState _onUserUpdate(AppState state, OnUserUpdateAction action) {
  return state.rebuild((a) => a..user = action.user.toBuilder());
}
