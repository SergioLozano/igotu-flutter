import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:igotu/models/bid.dart';
import 'package:igotu/models/participant.dart';
import 'package:igotu/models/phone_auth.dart';
import 'package:igotu/models/product.dart';
import 'package:igotu/models/user.dart';

// ignore: prefer_double_quotes
part 'app_state.g.dart';

/// This class holds the whole application state.
/// Which can include:
/// - user calendar
/// - current user profile
/// - joined channels
/// - received messages
/// - etc.
///

abstract class AppState implements Built<AppState, AppStateBuilder> {
  @nullable
  PhoneAuth get phoneAuth;
  
  @nullable
  User get user;

  @nullable
  String get fcmToken;

  BuiltList<Product> get products;

  @nullable
  BuiltList<Bid> get bids;

  @nullable
  Product get product;

  @nullable
  BuiltList<Participant> get participants;

  AppState._();

  factory AppState([void Function(AppStateBuilder) updates]) = _$AppState;

  factory AppState.init() => AppState((a) => a
    ..products = ListBuilder()
    ..bids = ListBuilder()
    ..participants = ListBuilder());

  AppState clear() {
    // keep the temporal fcm token even when clearing state
    // so it can be set again on login.
    //
    // Add here anything else that also needs to be carried over.
    return AppState.init().rebuild((s) => s..fcmToken = fcmToken);
  }
}
