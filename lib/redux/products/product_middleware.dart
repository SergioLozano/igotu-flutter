import 'package:igotu/data/product_repository.dart';
import 'package:igotu/models/product.dart';
import 'package:igotu/redux/app_actions.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/products/product_actions.dart';
import 'package:igotu/redux/stream_subscriptions.dart';
import 'package:igotu/utils/logger.dart';
import 'package:redux/redux.dart';

List<Middleware<AppState>> createProductsMiddleware(
  ProductRepository productRepository,
) {
  return [
    TypedMiddleware<AppState, ConnectToDataSource>(
        _listenProducts(productRepository)),
    TypedMiddleware<AppState, SelectProductByIdAction>(
        _selectProductById(productRepository))
  ];
}

void Function(
  Store<AppState> store,
  ConnectToDataSource action,
  NextDispatcher next,
) _listenProducts(
  ProductRepository productRepository,
) {
  return (store, action, next) {
    next(action);
    try {
      productsSubscription?.cancel();
      productsSubscription =
          productRepository.getProductsStream().listen((products) {
        store.dispatch(LoadProducts(products: products));
      });
    } catch (e) {
      Logger.e("Failed to listen to products", e: e, s: StackTrace.current);
    }
  };
}

void Function(
  Store<AppState> store,
  SelectProductByIdAction action,
  NextDispatcher next,
) _selectProductById(ProductRepository productRepository) {
  return (store, action, next) async {
    next(action);

    try {
      final products = store.state.products;
      Product product;

      product =
          products.firstWhere((p) => p.id == action.productId, orElse: null);

      if (product == null) {
        product = await productRepository.getProduct(action.productId);
      }

      if (product != null) {
        store.dispatch(SelectProductById(product: product));
      }
    } catch (error) {
      Logger.e("Failed to load product by id", e: error, s: StackTrace.current);
    }
  };
}
