import 'package:built_collection/built_collection.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/products/product_actions.dart';
import 'package:redux/redux.dart';

final productReducers = <AppState Function(AppState, dynamic)>[
  TypedReducer<AppState, LoadProducts>(_onProductsLoaded),
  TypedReducer<AppState, SelectProductById>(_onProductSelect),
];

AppState _onProductsLoaded(AppState state, LoadProducts action) {
  return state.rebuild((a) => a..products = ListBuilder(action.products));
}

AppState _onProductSelect(AppState state, SelectProductById action) {
  return state.rebuild((a) => a..product = action.product.toBuilder());
}
