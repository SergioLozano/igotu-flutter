import 'package:igotu/models/product.dart';
import "package:meta/meta.dart";

@immutable
class LoadProducts {
  final List<Product> products;

  const LoadProducts({@required this.products});

  @override
  String toString() {
    return "ProductsUpdatedAction{products: $products}";
  }
}

@immutable
class SelectProductByIdAction {
  final String productId;

  const SelectProductByIdAction({
    @required this.productId,
  });
}

@immutable
class SelectProductById {
  final Product product;

  const SelectProductById({@required this.product});

  @override
  String toString() {
    return "SelectProductById{product: $product}";
  }
}
