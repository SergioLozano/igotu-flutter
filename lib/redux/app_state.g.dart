// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_state.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AppState extends AppState {
  @override
  final PhoneAuth phoneAuth;
  @override
  final User user;
  @override
  final String fcmToken;
  @override
  final BuiltList<Product> products;
  @override
  final BuiltList<Bid> bids;
  @override
  final Product product;
  @override
  final BuiltList<Participant> participants;

  factory _$AppState([void Function(AppStateBuilder) updates]) =>
      (new AppStateBuilder()..update(updates)).build();

  _$AppState._(
      {this.phoneAuth,
      this.user,
      this.fcmToken,
      this.products,
      this.bids,
      this.product,
      this.participants})
      : super._() {
    if (products == null) {
      throw new BuiltValueNullFieldError('AppState', 'products');
    }
  }

  @override
  AppState rebuild(void Function(AppStateBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AppStateBuilder toBuilder() => new AppStateBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AppState &&
        phoneAuth == other.phoneAuth &&
        user == other.user &&
        fcmToken == other.fcmToken &&
        products == other.products &&
        bids == other.bids &&
        product == other.product &&
        participants == other.participants;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, phoneAuth.hashCode), user.hashCode),
                        fcmToken.hashCode),
                    products.hashCode),
                bids.hashCode),
            product.hashCode),
        participants.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AppState')
          ..add('phoneAuth', phoneAuth)
          ..add('user', user)
          ..add('fcmToken', fcmToken)
          ..add('products', products)
          ..add('bids', bids)
          ..add('product', product)
          ..add('participants', participants))
        .toString();
  }
}

class AppStateBuilder implements Builder<AppState, AppStateBuilder> {
  _$AppState _$v;

  PhoneAuthBuilder _phoneAuth;
  PhoneAuthBuilder get phoneAuth =>
      _$this._phoneAuth ??= new PhoneAuthBuilder();
  set phoneAuth(PhoneAuthBuilder phoneAuth) => _$this._phoneAuth = phoneAuth;

  UserBuilder _user;
  UserBuilder get user => _$this._user ??= new UserBuilder();
  set user(UserBuilder user) => _$this._user = user;

  String _fcmToken;
  String get fcmToken => _$this._fcmToken;
  set fcmToken(String fcmToken) => _$this._fcmToken = fcmToken;

  ListBuilder<Product> _products;
  ListBuilder<Product> get products =>
      _$this._products ??= new ListBuilder<Product>();
  set products(ListBuilder<Product> products) => _$this._products = products;

  ListBuilder<Bid> _bids;
  ListBuilder<Bid> get bids => _$this._bids ??= new ListBuilder<Bid>();
  set bids(ListBuilder<Bid> bids) => _$this._bids = bids;

  ProductBuilder _product;
  ProductBuilder get product => _$this._product ??= new ProductBuilder();
  set product(ProductBuilder product) => _$this._product = product;

  ListBuilder<Participant> _participants;
  ListBuilder<Participant> get participants =>
      _$this._participants ??= new ListBuilder<Participant>();
  set participants(ListBuilder<Participant> participants) =>
      _$this._participants = participants;

  AppStateBuilder();

  AppStateBuilder get _$this {
    if (_$v != null) {
      _phoneAuth = _$v.phoneAuth?.toBuilder();
      _user = _$v.user?.toBuilder();
      _fcmToken = _$v.fcmToken;
      _products = _$v.products?.toBuilder();
      _bids = _$v.bids?.toBuilder();
      _product = _$v.product?.toBuilder();
      _participants = _$v.participants?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AppState other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$AppState;
  }

  @override
  void update(void Function(AppStateBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AppState build() {
    _$AppState _$result;
    try {
      _$result = _$v ??
          new _$AppState._(
              phoneAuth: _phoneAuth?.build(),
              user: _user?.build(),
              fcmToken: fcmToken,
              products: products.build(),
              bids: _bids?.build(),
              product: _product?.build(),
              participants: _participants?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'phoneAuth';
        _phoneAuth?.build();
        _$failedField = 'user';
        _user?.build();

        _$failedField = 'products';
        products.build();
        _$failedField = 'bids';
        _bids?.build();
        _$failedField = 'product';
        _product?.build();
        _$failedField = 'participants';
        _participants?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'AppState', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
