import 'package:flutter/material.dart';

class AppTheme {
  static const pixelMultiplier = 1.0;
  static const appMargin = 12.0 * pixelMultiplier;
  static const avatarSize = 36.0 * pixelMultiplier;
  static const appBarSize = 50.0;

  static const fontFamilyNeusaNextStdBold = "NeusaNextStd-Bold";
  static const fontFamilyNeusaNextStdMedium = "NeusaNextStd-Medium";
  static const fontFamilyNeusaNextStdRegular = "NeusaNextStd-Regular";
  static const fontFamilyNeusaNextStdLight = "NeusaNextStd-Light";

  static final colorLightPurple = Colors.deepPurple[400];
  static final colorDarkPurple = Colors.deepPurple[700];
  static final colorDarkerPurple = Colors.deepPurple[900];
  static const colorDarkPurpleFont = Color.fromRGBO(21, 8, 80, 1.0);
  static const colorShadow = Color.fromRGBO(204, 204, 204, 1.0);
  static const colorTextDisabled = Color.fromRGBO(153, 153, 153, 1.0);
  static const colorTextEnabled = Color.fromRGBO(0, 0, 0, 1.0);
  static const colorTextLink = Color.fromRGBO(74, 144, 226, 1.0);
  static const colorGrey128 = Color.fromRGBO(128, 128, 128, 1.0);
  static const colorGrey128_25 = Color.fromRGBO(128, 128, 128, 0.25);
  static const colorGrey128_50 = Color.fromRGBO(128, 128, 128, 0.5);
  static const colorGrey155 = Color.fromRGBO(155, 155, 155, 1.0);
  static const colorGrey225 = Color.fromRGBO(225, 225, 225, 1.0);
  static const colorGrey241 = Color.fromRGBO(241, 241, 241, 1.0);
  static final colorWhite_50 = Colors.white.withOpacity(0.5);

  static ThemeData get theme {
    return ThemeData.light().copyWith(
      primaryColorLight: Colors.blueGrey,
      primaryColor: Colors.blueGrey[800],
      accentColor: colorDarkPurple,
    );
  }

  static TextStyle get buttonTextStyle {
    return TextStyle(
        fontSize: 16 * pixelMultiplier, fontFamily: fontFamilyNeusaNextStdBold);
  }

  static TextStyle get buttonMediumTextStyle {
    return TextStyle(
      fontSize: 16 * pixelMultiplier,
      fontFamily: fontFamilyNeusaNextStdMedium,
    );
  }

  static TextStyle get inputHintTextStyle {
    return TextStyle(
      fontSize: 16 * pixelMultiplier,
      fontFamily: fontFamilyNeusaNextStdRegular,
      color: colorTextDisabled,
    );
  }

  static TextStyle get inputTextStyle {
    return TextStyle(
      fontSize: 16 * pixelMultiplier,
      fontFamily: fontFamilyNeusaNextStdRegular,
      color: colorTextEnabled,
    );
  }

  static TextStyle get optionTextStyle {
    return TextStyle(
      fontSize: 20,
      fontFamily: fontFamilyNeusaNextStdMedium,
      color: colorTextEnabled,
    );
  }

  static TextStyle get linkTextStyle {
    return TextStyle(
      fontSize: 16 * pixelMultiplier,
      fontFamily: fontFamilyNeusaNextStdRegular,
      color: colorTextLink,
    );
  }

  static TextStyle get appBarTitleTextStyle {
    return TextStyle(
      fontSize: 20,
      fontFamily: fontFamilyNeusaNextStdMedium,
      color: colorTextEnabled,
    );
  }

  static TextStyle get appBarTitle2TextStyle {
    return TextStyle(
      fontSize: 16,
      fontFamily: fontFamilyNeusaNextStdMedium,
      color: colorTextEnabled,
    );
  }

  static TextStyle get appBarSubtitleTextStyle {
    return TextStyle(
      fontSize: 16,
      fontFamily: fontFamilyNeusaNextStdRegular,
      color: colorTextEnabled,
    );
  }

  static TextStyle get appBarActionTextStyle {
    return TextStyle(
      fontSize: 16,
      fontFamily: fontFamilyNeusaNextStdBold,
      color: colorTextEnabled,
    );
  }

  static TextStyle get appBarActionDisabledTextStyle {
    return TextStyle(
      fontSize: 16,
      fontFamily: fontFamilyNeusaNextStdBold,
      color: colorTextDisabled,
    );
  }

  static TextStyle get inputMediumTextStyle {
    return TextStyle(
      fontSize: 16,
      fontFamily: fontFamilyNeusaNextStdMedium,
      color: colorTextEnabled,
    );
  }
}
