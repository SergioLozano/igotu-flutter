// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$User extends User {
  @override
  final String uid;
  @override
  final String email;
  @override
  final String phone;
  @override
  final String username;
  @override
  final String image;
  @override
  final int tickets;
  @override
  final int goldenTickets;
  @override
  final int exp;

  factory _$User([void Function(UserBuilder) updates]) =>
      (new UserBuilder()..update(updates)).build();

  _$User._(
      {this.uid,
      this.email,
      this.phone,
      this.username,
      this.image,
      this.tickets,
      this.goldenTickets,
      this.exp})
      : super._() {
    if (uid == null) {
      throw new BuiltValueNullFieldError('User', 'uid');
    }
    if (username == null) {
      throw new BuiltValueNullFieldError('User', 'username');
    }
    if (image == null) {
      throw new BuiltValueNullFieldError('User', 'image');
    }
    if (tickets == null) {
      throw new BuiltValueNullFieldError('User', 'tickets');
    }
    if (goldenTickets == null) {
      throw new BuiltValueNullFieldError('User', 'goldenTickets');
    }
    if (exp == null) {
      throw new BuiltValueNullFieldError('User', 'exp');
    }
  }

  @override
  User rebuild(void Function(UserBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserBuilder toBuilder() => new UserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is User &&
        uid == other.uid &&
        email == other.email &&
        phone == other.phone &&
        username == other.username &&
        image == other.image &&
        tickets == other.tickets &&
        goldenTickets == other.goldenTickets &&
        exp == other.exp;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, uid.hashCode), email.hashCode),
                            phone.hashCode),
                        username.hashCode),
                    image.hashCode),
                tickets.hashCode),
            goldenTickets.hashCode),
        exp.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('User')
          ..add('uid', uid)
          ..add('email', email)
          ..add('phone', phone)
          ..add('username', username)
          ..add('image', image)
          ..add('tickets', tickets)
          ..add('goldenTickets', goldenTickets)
          ..add('exp', exp))
        .toString();
  }
}

class UserBuilder implements Builder<User, UserBuilder> {
  _$User _$v;

  String _uid;
  String get uid => _$this._uid;
  set uid(String uid) => _$this._uid = uid;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _phone;
  String get phone => _$this._phone;
  set phone(String phone) => _$this._phone = phone;

  String _username;
  String get username => _$this._username;
  set username(String username) => _$this._username = username;

  String _image;
  String get image => _$this._image;
  set image(String image) => _$this._image = image;

  int _tickets;
  int get tickets => _$this._tickets;
  set tickets(int tickets) => _$this._tickets = tickets;

  int _goldenTickets;
  int get goldenTickets => _$this._goldenTickets;
  set goldenTickets(int goldenTickets) => _$this._goldenTickets = goldenTickets;

  int _exp;
  int get exp => _$this._exp;
  set exp(int exp) => _$this._exp = exp;

  UserBuilder();

  UserBuilder get _$this {
    if (_$v != null) {
      _uid = _$v.uid;
      _email = _$v.email;
      _phone = _$v.phone;
      _username = _$v.username;
      _image = _$v.image;
      _tickets = _$v.tickets;
      _goldenTickets = _$v.goldenTickets;
      _exp = _$v.exp;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(User other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$User;
  }

  @override
  void update(void Function(UserBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$User build() {
    final _$result = _$v ??
        new _$User._(
            uid: uid,
            email: email,
            phone: phone,
            username: username,
            image: image,
            tickets: tickets,
            goldenTickets: goldenTickets,
            exp: exp);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
