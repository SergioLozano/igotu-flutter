import 'package:built_value/built_value.dart';

// ignore: prefer_double_quotes
part 'bid.g.dart';

abstract class Bid implements Built<Bid, BidBuilder> {
  @nullable
  String get id;

  String get productId;

  double get amount;

  DateTime get timestamp;

  Bid._();

  factory Bid([void Function(BidBuilder) updates]) = _$Bid;
}

/// Custom Builder to allow defaults
abstract class BidBuilder implements Builder<Bid, BidBuilder> {
  @nullable
  String id;

  String productId;

  double amount;

  DateTime timestamp = DateTime.now();

  factory BidBuilder() = _$BidBuilder;
  BidBuilder._();
}
