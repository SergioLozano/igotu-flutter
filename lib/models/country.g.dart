// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'country.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Country extends Country {
  @override
  final String name;
  @override
  final String flag;
  @override
  final String code;
  @override
  final String dialCode;

  factory _$Country([void Function(CountryBuilder) updates]) =>
      (new CountryBuilder()..update(updates)).build();

  _$Country._({this.name, this.flag, this.code, this.dialCode}) : super._() {
    if (name == null) {
      throw new BuiltValueNullFieldError('Country', 'name');
    }
    if (flag == null) {
      throw new BuiltValueNullFieldError('Country', 'flag');
    }
    if (code == null) {
      throw new BuiltValueNullFieldError('Country', 'code');
    }
    if (dialCode == null) {
      throw new BuiltValueNullFieldError('Country', 'dialCode');
    }
  }

  @override
  Country rebuild(void Function(CountryBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CountryBuilder toBuilder() => new CountryBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Country &&
        name == other.name &&
        flag == other.flag &&
        code == other.code &&
        dialCode == other.dialCode;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, name.hashCode), flag.hashCode), code.hashCode),
        dialCode.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Country')
          ..add('name', name)
          ..add('flag', flag)
          ..add('code', code)
          ..add('dialCode', dialCode))
        .toString();
  }
}

class CountryBuilder implements Builder<Country, CountryBuilder> {
  _$Country _$v;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _flag;
  String get flag => _$this._flag;
  set flag(String flag) => _$this._flag = flag;

  String _code;
  String get code => _$this._code;
  set code(String code) => _$this._code = code;

  String _dialCode;
  String get dialCode => _$this._dialCode;
  set dialCode(String dialCode) => _$this._dialCode = dialCode;

  CountryBuilder();

  CountryBuilder get _$this {
    if (_$v != null) {
      _name = _$v.name;
      _flag = _$v.flag;
      _code = _$v.code;
      _dialCode = _$v.dialCode;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Country other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Country;
  }

  @override
  void update(void Function(CountryBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Country build() {
    final _$result = _$v ??
        new _$Country._(name: name, flag: flag, code: code, dialCode: dialCode);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
