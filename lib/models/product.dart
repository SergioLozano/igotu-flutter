import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';

// ignore: prefer_double_quotes
part 'product.g.dart';

abstract class Product implements Built<Product, ProductBuilder> {
  String get id;

  String get name;

  String get description;

  String get coverUrl;

  BuiltList<String> get images;

  DateTime get timestamp;

  num get quantity;

  int get tickets;

  Product._();

  factory Product([void Function(ProductBuilder) updates]) = _$Product;
}