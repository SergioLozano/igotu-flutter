import 'package:built_value/built_value.dart';

// ignore: prefer_double_quotes
part 'participant.g.dart';

abstract class Participant implements Built<Participant, ParticipantBuilder> {
  String get uid;

  String get username;

  @nullable
  String get image;

  int get tickets;

  Participant._();

  factory Participant([void Function(ParticipantBuilder) updates]) = _$Participant;
}