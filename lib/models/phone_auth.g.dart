// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_auth.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PhoneAuth extends PhoneAuth {
  @override
  final AuthStatus authStatus;
  @override
  final String verificationId;
  @override
  final String error;

  factory _$PhoneAuth([void Function(PhoneAuthBuilder) updates]) =>
      (new PhoneAuthBuilder()..update(updates)).build() as _$PhoneAuth;

  _$PhoneAuth._({this.authStatus, this.verificationId, this.error})
      : super._() {
    if (authStatus == null) {
      throw new BuiltValueNullFieldError('PhoneAuth', 'authStatus');
    }
    if (error == null) {
      throw new BuiltValueNullFieldError('PhoneAuth', 'error');
    }
  }

  @override
  PhoneAuth rebuild(void Function(PhoneAuthBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  _$PhoneAuthBuilder toBuilder() => new _$PhoneAuthBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PhoneAuth &&
        authStatus == other.authStatus &&
        verificationId == other.verificationId &&
        error == other.error;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, authStatus.hashCode), verificationId.hashCode),
        error.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PhoneAuth')
          ..add('authStatus', authStatus)
          ..add('verificationId', verificationId)
          ..add('error', error))
        .toString();
  }
}

class _$PhoneAuthBuilder extends PhoneAuthBuilder {
  _$PhoneAuth _$v;

  @override
  AuthStatus get authStatus {
    _$this;
    return super.authStatus;
  }

  @override
  set authStatus(AuthStatus authStatus) {
    _$this;
    super.authStatus = authStatus;
  }

  @override
  String get verificationId {
    _$this;
    return super.verificationId;
  }

  @override
  set verificationId(String verificationId) {
    _$this;
    super.verificationId = verificationId;
  }

  @override
  String get error {
    _$this;
    return super.error;
  }

  @override
  set error(String error) {
    _$this;
    super.error = error;
  }

  _$PhoneAuthBuilder() : super._();

  PhoneAuthBuilder get _$this {
    if (_$v != null) {
      super.authStatus = _$v.authStatus;
      super.verificationId = _$v.verificationId;
      super.error = _$v.error;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PhoneAuth other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PhoneAuth;
  }

  @override
  void update(void Function(PhoneAuthBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PhoneAuth build() {
    final _$result = _$v ??
        new _$PhoneAuth._(
            authStatus: authStatus,
            verificationId: verificationId,
            error: error);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
