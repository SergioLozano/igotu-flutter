// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'participant.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Participant extends Participant {
  @override
  final String uid;
  @override
  final String username;
  @override
  final String image;
  @override
  final int tickets;

  factory _$Participant([void Function(ParticipantBuilder) updates]) =>
      (new ParticipantBuilder()..update(updates)).build();

  _$Participant._({this.uid, this.username, this.image, this.tickets})
      : super._() {
    if (uid == null) {
      throw new BuiltValueNullFieldError('Participant', 'uid');
    }
    if (username == null) {
      throw new BuiltValueNullFieldError('Participant', 'username');
    }
    if (tickets == null) {
      throw new BuiltValueNullFieldError('Participant', 'tickets');
    }
  }

  @override
  Participant rebuild(void Function(ParticipantBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ParticipantBuilder toBuilder() => new ParticipantBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Participant &&
        uid == other.uid &&
        username == other.username &&
        image == other.image &&
        tickets == other.tickets;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, uid.hashCode), username.hashCode), image.hashCode),
        tickets.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Participant')
          ..add('uid', uid)
          ..add('username', username)
          ..add('image', image)
          ..add('tickets', tickets))
        .toString();
  }
}

class ParticipantBuilder implements Builder<Participant, ParticipantBuilder> {
  _$Participant _$v;

  String _uid;
  String get uid => _$this._uid;
  set uid(String uid) => _$this._uid = uid;

  String _username;
  String get username => _$this._username;
  set username(String username) => _$this._username = username;

  String _image;
  String get image => _$this._image;
  set image(String image) => _$this._image = image;

  int _tickets;
  int get tickets => _$this._tickets;
  set tickets(int tickets) => _$this._tickets = tickets;

  ParticipantBuilder();

  ParticipantBuilder get _$this {
    if (_$v != null) {
      _uid = _$v.uid;
      _username = _$v.username;
      _image = _$v.image;
      _tickets = _$v.tickets;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Participant other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Participant;
  }

  @override
  void update(void Function(ParticipantBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Participant build() {
    final _$result = _$v ??
        new _$Participant._(
            uid: uid, username: username, image: image, tickets: tickets);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
