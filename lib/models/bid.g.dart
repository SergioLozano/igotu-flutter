// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bid.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Bid extends Bid {
  @override
  final String id;
  @override
  final String productId;
  @override
  final double amount;
  @override
  final DateTime timestamp;

  factory _$Bid([void Function(BidBuilder) updates]) =>
      (new BidBuilder()..update(updates)).build() as _$Bid;

  _$Bid._({this.id, this.productId, this.amount, this.timestamp}) : super._() {
    if (productId == null) {
      throw new BuiltValueNullFieldError('Bid', 'productId');
    }
    if (amount == null) {
      throw new BuiltValueNullFieldError('Bid', 'amount');
    }
    if (timestamp == null) {
      throw new BuiltValueNullFieldError('Bid', 'timestamp');
    }
  }

  @override
  Bid rebuild(void Function(BidBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  _$BidBuilder toBuilder() => new _$BidBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Bid &&
        id == other.id &&
        productId == other.productId &&
        amount == other.amount &&
        timestamp == other.timestamp;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), productId.hashCode), amount.hashCode),
        timestamp.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Bid')
          ..add('id', id)
          ..add('productId', productId)
          ..add('amount', amount)
          ..add('timestamp', timestamp))
        .toString();
  }
}

class _$BidBuilder extends BidBuilder {
  _$Bid _$v;

  @override
  String get id {
    _$this;
    return super.id;
  }

  @override
  set id(String id) {
    _$this;
    super.id = id;
  }

  @override
  String get productId {
    _$this;
    return super.productId;
  }

  @override
  set productId(String productId) {
    _$this;
    super.productId = productId;
  }

  @override
  double get amount {
    _$this;
    return super.amount;
  }

  @override
  set amount(double amount) {
    _$this;
    super.amount = amount;
  }

  @override
  DateTime get timestamp {
    _$this;
    return super.timestamp;
  }

  @override
  set timestamp(DateTime timestamp) {
    _$this;
    super.timestamp = timestamp;
  }

  _$BidBuilder() : super._();

  BidBuilder get _$this {
    if (_$v != null) {
      super.id = _$v.id;
      super.productId = _$v.productId;
      super.amount = _$v.amount;
      super.timestamp = _$v.timestamp;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Bid other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Bid;
  }

  @override
  void update(void Function(BidBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Bid build() {
    final _$result = _$v ??
        new _$Bid._(
            id: id, productId: productId, amount: amount, timestamp: timestamp);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
