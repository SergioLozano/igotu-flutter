import "package:built_value/built_value.dart";

// ignore: prefer_double_quotes
part 'user.g.dart';

abstract class User implements Built<User, UserBuilder> {
  String get uid;

  @nullable
  String get email;

  @nullable
  String get phone;

  String get username;

  String get image;

  int get tickets;

  int get goldenTickets;

  int get exp;

  User._();

  factory User([void Function(UserBuilder) updates]) = _$User;
}
