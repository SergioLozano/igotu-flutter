// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Product extends Product {
  @override
  final String id;
  @override
  final String name;
  @override
  final String description;
  @override
  final String coverUrl;
  @override
  final BuiltList<String> images;
  @override
  final DateTime timestamp;
  @override
  final num quantity;
  @override
  final int tickets;

  factory _$Product([void Function(ProductBuilder) updates]) =>
      (new ProductBuilder()..update(updates)).build();

  _$Product._(
      {this.id,
      this.name,
      this.description,
      this.coverUrl,
      this.images,
      this.timestamp,
      this.quantity,
      this.tickets})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('Product', 'id');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('Product', 'name');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('Product', 'description');
    }
    if (coverUrl == null) {
      throw new BuiltValueNullFieldError('Product', 'coverUrl');
    }
    if (images == null) {
      throw new BuiltValueNullFieldError('Product', 'images');
    }
    if (timestamp == null) {
      throw new BuiltValueNullFieldError('Product', 'timestamp');
    }
    if (quantity == null) {
      throw new BuiltValueNullFieldError('Product', 'quantity');
    }
    if (tickets == null) {
      throw new BuiltValueNullFieldError('Product', 'tickets');
    }
  }

  @override
  Product rebuild(void Function(ProductBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProductBuilder toBuilder() => new ProductBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Product &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        coverUrl == other.coverUrl &&
        images == other.images &&
        timestamp == other.timestamp &&
        quantity == other.quantity &&
        tickets == other.tickets;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, id.hashCode), name.hashCode),
                            description.hashCode),
                        coverUrl.hashCode),
                    images.hashCode),
                timestamp.hashCode),
            quantity.hashCode),
        tickets.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Product')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('coverUrl', coverUrl)
          ..add('images', images)
          ..add('timestamp', timestamp)
          ..add('quantity', quantity)
          ..add('tickets', tickets))
        .toString();
  }
}

class ProductBuilder implements Builder<Product, ProductBuilder> {
  _$Product _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _coverUrl;
  String get coverUrl => _$this._coverUrl;
  set coverUrl(String coverUrl) => _$this._coverUrl = coverUrl;

  ListBuilder<String> _images;
  ListBuilder<String> get images =>
      _$this._images ??= new ListBuilder<String>();
  set images(ListBuilder<String> images) => _$this._images = images;

  DateTime _timestamp;
  DateTime get timestamp => _$this._timestamp;
  set timestamp(DateTime timestamp) => _$this._timestamp = timestamp;

  num _quantity;
  num get quantity => _$this._quantity;
  set quantity(num quantity) => _$this._quantity = quantity;

  int _tickets;
  int get tickets => _$this._tickets;
  set tickets(int tickets) => _$this._tickets = tickets;

  ProductBuilder();

  ProductBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _description = _$v.description;
      _coverUrl = _$v.coverUrl;
      _images = _$v.images?.toBuilder();
      _timestamp = _$v.timestamp;
      _quantity = _$v.quantity;
      _tickets = _$v.tickets;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Product other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Product;
  }

  @override
  void update(void Function(ProductBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Product build() {
    _$Product _$result;
    try {
      _$result = _$v ??
          new _$Product._(
              id: id,
              name: name,
              description: description,
              coverUrl: coverUrl,
              images: images.build(),
              timestamp: timestamp,
              quantity: quantity,
              tickets: tickets);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'images';
        images.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Product', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
