import 'package:built_value/built_value.dart';

// ignore: prefer_double_quotes
part 'country.g.dart';

abstract class Country implements Built<Country, CountryBuilder> {
  String get name;

  String get flag;

  String get code;

  String get dialCode;

  Country._();

  factory Country([void Function(CountryBuilder) updates]) = _$Country;
}

