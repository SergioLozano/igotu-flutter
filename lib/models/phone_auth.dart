import 'package:built_value/built_value.dart';

// ignore: prefer_double_quotes
part 'phone_auth.g.dart';

abstract class PhoneAuth implements Built<PhoneAuth, PhoneAuthBuilder> {
  AuthStatus get authStatus;

  @nullable
  String get verificationId;

  String get error;

  PhoneAuth._();

  factory PhoneAuth([void Function(PhoneAuthBuilder) updates]) = _$PhoneAuth;
}

/// Custom Builder to allow defaults
abstract class PhoneAuthBuilder implements Builder<PhoneAuth, PhoneAuthBuilder> {
  AuthStatus authStatus = AuthStatus.LOADING;

  @nullable
  String verificationId;

  String error = "";

  factory PhoneAuthBuilder() = _$PhoneAuthBuilder;
  PhoneAuthBuilder._();
}

enum AuthStatus { PHONE, SMS, LOADING }

class AuthStatusHelper {
  static String stringOf(AuthStatus authStatus) {
    switch (authStatus) {
      case AuthStatus.PHONE:
        return "PHONE";
      case AuthStatus.SMS:
        return "SMS";
      case AuthStatus.LOADING:
        return "LOADING";
      default:
        return "PHONE";
    }
  }

  static AuthStatus valueOf(String string) {
    switch (string) {
      case "PHONE":
        return AuthStatus.PHONE;
        break;
      case "SMS":
        return AuthStatus.SMS;
        break;
      case "LOADING":
        return AuthStatus.LOADING;
        break;
      default:
        return AuthStatus.PHONE;
        break;
    }
  }
}
