import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:igotu/pages/home/home_screen.dart';
import 'package:igotu/pages/home/main_screen_viewmodel.dart';
import 'package:igotu/redux/app_state.dart';

///
/// This screen loads the HomeScreen when there's data loaded
/// to avoid things like having null User, null Channel, etc.
///
/// Also holds the ValueNotifier for the side open/closed state
/// as it passes it to the SlideOut widget and the HomeScreen.
///

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, MainScreenViewModel>(
      distinct: true,
      converter: MainScreenViewModel.fromStore,
      builder: (context, vm) {
        if (vm.hasData) {
          return HomeScreen();
        } else {
          // TODO: Proper empty state screen
          return Scaffold();
        }
      },
    );
  }
}