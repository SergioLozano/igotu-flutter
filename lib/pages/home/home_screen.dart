import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:igotu/pages/home/page_views/products.dart';
import 'package:igotu/pages/home/page_views/profile.dart';
import 'package:igotu/pages/home/page_views/ranking.dart';
import 'package:igotu/pages/home/page_views/share.dart';
import 'package:igotu/pages/home/page_views/shop.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:igotu/theme.dart';

final usersRef = Firestore.instance.collection('users');
final productsRef = Firestore.instance.collection('products');
final bidsRef = Firestore.instance.collection('bids');
final participatingRef = Firestore.instance.collection('participating');

final DateTime timestamp = DateTime.now();

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    Key key,
  }) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> {
  final _scaffoldkey = GlobalKey<ScaffoldState>();
  PageController pageController;
  int pageIndex = 2;
  GlobalKey _bottomNavigationKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    pageController = PageController(initialPage: 2);
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  onPageChanged(int pageIndex) {
    setState(() {
      this.pageIndex = pageIndex;
    });
    print(this.pageIndex);
  }

  onTap(int pageIndex) {
    onPageChanged(pageIndex);
    pageController.animateToPage(pageIndex,
        duration: Duration(milliseconds: 600), curve: Curves.easeInOut);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF6F7F8),
      key: _scaffoldkey,
      body: Container(
        //padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: PageView(
          children: <Widget>[
            Share(),
            Shop(),
            Products(),
            Ranking(),
            Profile(),
          ],
          controller: pageController,
          //onPageChanged: onPageChanged,
          physics: NeverScrollableScrollPhysics(),
        ),
      ),
      bottomNavigationBar: CurvedNavigationBar(
        key: _bottomNavigationKey,
        index: pageIndex,
        height: 50.0,
        onTap: onTap,
        items: <Widget>[
          Icon(
            AntDesign.sharealt,
            size: 24,
            color: Colors.white,
          ),
          Icon(
            Foundation.ticket,
            size: 24,
            color: Colors.white,
          ),
          Icon(
            AntDesign.home,
            size: 24,
            color: Colors.white,
          ),
          Icon(
            AntDesign.Trophy,
            size: 24,
            color: Colors.white,
          ),
          Icon(
            AntDesign.user,
            size: 24,
            color: Colors.white,
          ),
        ],
        color: AppTheme.colorDarkPurple,
        buttonBackgroundColor: AppTheme.colorDarkPurple,
        backgroundColor: AppTheme.colorLightPurple,
        animationCurve: Curves.easeInOut,
        animationDuration: Duration(milliseconds: 600),
      ),
    );
  }
}
