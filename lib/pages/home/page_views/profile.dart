import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/authentication/auth_actions.dart';
import 'package:igotu/theme.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    final borderRadius = BorderRadius.circular(24);
    return Center(
      child: Material(
        color: Colors.transparent,
        borderRadius: borderRadius,
        child: InkWell(
          onTap: () {
            StoreProvider.of<AppState>(context).dispatch(LogOutAction());
            Navigator.pop(context);
          },
          borderRadius: borderRadius,
          child: Container(
            decoration: BoxDecoration(
                borderRadius: borderRadius,
                border: Border.all(color: Colors.red)),
            height: 40,
            width: 200,
            child: Center(
              child: Text(
                "Log Out",
                style: AppTheme.buttonTextStyle.apply(color: Colors.red),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
