import 'package:flutter/material.dart';
import 'package:igotu/widgets/common/header/header.dart';
import 'package:igotu/widgets/product/product_list.dart';

class Products extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: <Widget>[
          Header(),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.0),
            child: ProductList(),
          )
        ],
      ),
    );
  }
}
