// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_screen_viewmodel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ProductScreenViewModel extends ProductScreenViewModel {
  @override
  final String id;
  @override
  final String name;
  @override
  final String description;
  @override
  final String coverUrl;
  @override
  final BuiltList<String> images;
  @override
  final DateTime datetime;
  @override
  final num quantity;
  @override
  final int productTickets;
  @override
  final Participant userParticipation;
  @override
  final int userTickets;

  factory _$ProductScreenViewModel(
          [void Function(ProductScreenViewModelBuilder) updates]) =>
      (new ProductScreenViewModelBuilder()..update(updates)).build();

  _$ProductScreenViewModel._(
      {this.id,
      this.name,
      this.description,
      this.coverUrl,
      this.images,
      this.datetime,
      this.quantity,
      this.productTickets,
      this.userParticipation,
      this.userTickets})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('ProductScreenViewModel', 'id');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('ProductScreenViewModel', 'name');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError(
          'ProductScreenViewModel', 'description');
    }
    if (coverUrl == null) {
      throw new BuiltValueNullFieldError('ProductScreenViewModel', 'coverUrl');
    }
    if (images == null) {
      throw new BuiltValueNullFieldError('ProductScreenViewModel', 'images');
    }
    if (datetime == null) {
      throw new BuiltValueNullFieldError('ProductScreenViewModel', 'datetime');
    }
    if (quantity == null) {
      throw new BuiltValueNullFieldError('ProductScreenViewModel', 'quantity');
    }
    if (productTickets == null) {
      throw new BuiltValueNullFieldError(
          'ProductScreenViewModel', 'productTickets');
    }
    if (userTickets == null) {
      throw new BuiltValueNullFieldError(
          'ProductScreenViewModel', 'userTickets');
    }
  }

  @override
  ProductScreenViewModel rebuild(
          void Function(ProductScreenViewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ProductScreenViewModelBuilder toBuilder() =>
      new ProductScreenViewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ProductScreenViewModel &&
        id == other.id &&
        name == other.name &&
        description == other.description &&
        coverUrl == other.coverUrl &&
        images == other.images &&
        datetime == other.datetime &&
        quantity == other.quantity &&
        productTickets == other.productTickets &&
        userParticipation == other.userParticipation &&
        userTickets == other.userTickets;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc($jc(0, id.hashCode), name.hashCode),
                                    description.hashCode),
                                coverUrl.hashCode),
                            images.hashCode),
                        datetime.hashCode),
                    quantity.hashCode),
                productTickets.hashCode),
            userParticipation.hashCode),
        userTickets.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ProductScreenViewModel')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('coverUrl', coverUrl)
          ..add('images', images)
          ..add('datetime', datetime)
          ..add('quantity', quantity)
          ..add('productTickets', productTickets)
          ..add('userParticipation', userParticipation)
          ..add('userTickets', userTickets))
        .toString();
  }
}

class ProductScreenViewModelBuilder
    implements Builder<ProductScreenViewModel, ProductScreenViewModelBuilder> {
  _$ProductScreenViewModel _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _coverUrl;
  String get coverUrl => _$this._coverUrl;
  set coverUrl(String coverUrl) => _$this._coverUrl = coverUrl;

  ListBuilder<String> _images;
  ListBuilder<String> get images =>
      _$this._images ??= new ListBuilder<String>();
  set images(ListBuilder<String> images) => _$this._images = images;

  DateTime _datetime;
  DateTime get datetime => _$this._datetime;
  set datetime(DateTime datetime) => _$this._datetime = datetime;

  num _quantity;
  num get quantity => _$this._quantity;
  set quantity(num quantity) => _$this._quantity = quantity;

  int _productTickets;
  int get productTickets => _$this._productTickets;
  set productTickets(int productTickets) =>
      _$this._productTickets = productTickets;

  ParticipantBuilder _userParticipation;
  ParticipantBuilder get userParticipation =>
      _$this._userParticipation ??= new ParticipantBuilder();
  set userParticipation(ParticipantBuilder userParticipation) =>
      _$this._userParticipation = userParticipation;

  int _userTickets;
  int get userTickets => _$this._userTickets;
  set userTickets(int userTickets) => _$this._userTickets = userTickets;

  ProductScreenViewModelBuilder();

  ProductScreenViewModelBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _description = _$v.description;
      _coverUrl = _$v.coverUrl;
      _images = _$v.images?.toBuilder();
      _datetime = _$v.datetime;
      _quantity = _$v.quantity;
      _productTickets = _$v.productTickets;
      _userParticipation = _$v.userParticipation?.toBuilder();
      _userTickets = _$v.userTickets;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ProductScreenViewModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ProductScreenViewModel;
  }

  @override
  void update(void Function(ProductScreenViewModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ProductScreenViewModel build() {
    _$ProductScreenViewModel _$result;
    try {
      _$result = _$v ??
          new _$ProductScreenViewModel._(
              id: id,
              name: name,
              description: description,
              coverUrl: coverUrl,
              images: images.build(),
              datetime: datetime,
              quantity: quantity,
              productTickets: productTickets,
              userParticipation: _userParticipation?.build(),
              userTickets: userTickets);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'images';
        images.build();

        _$failedField = 'userParticipation';
        _userParticipation?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ProductScreenViewModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
