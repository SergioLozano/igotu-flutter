import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_svg/svg.dart';
import 'package:igotu/igotu_localization.dart';
import 'package:igotu/pages/product/product_screen_viewmodel.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/theme.dart';
import 'package:igotu/utils/utils.dart';
import 'package:igotu/widgets/common/custom_listview.dart';
import 'package:igotu/widgets/product/participants/participants_list.dart';
import 'package:igotu/widgets/product/product_bid_dialog.dart';
import 'package:igotu/widgets/product/product_countdown.dart';
import 'package:igotu/widgets/product/product_description.dart';
import 'package:igotu/widgets/product/product_image_carousel.dart';
import 'package:igotu/widgets/product/product_quantity_chip.dart';

class ProductScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //final Product product = ModalRoute.of(context).settings.arguments;
    return StoreConnector<AppState, ProductScreenViewModel>(
      converter: ProductScreenViewModel.fromStore,
      distinct: true,
      builder: (context, vm) {
        return Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(AppTheme.appBarSize),
              child: AppBar(
                title: Text(
                  IgotuLocalizations.of(context).prizeDetailTitle,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.white, fontSize: 15.0),
                ),
                centerTitle: true,
                backgroundColor: Colors.deepPurple[700],
              ),
            ),
            body: _buildProductBody(context, vm));
      },
    );
  }

  Widget _buildProductBody(BuildContext context, ProductScreenViewModel vm) {
    return Stack(children: <Widget>[
      ScrollConfiguration(
        behavior: CustomListView(),
        child: ListView(
          children: <Widget>[
            Container(
              child:
                  Column(mainAxisAlignment: MainAxisAlignment.start, children: <
                      Widget>[
                Container(
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      ProductCarousel(
                        images: vm.images.asList(),
                      ),
                      Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 16.0, vertical: 8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Expanded(
                                  flex: 3,
                                  child: Container(
                                    margin:
                                        EdgeInsets.only(right: 24.0, top: 4.0),
                                    child: Text(
                                      vm.name,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Theme.of(context).primaryColor),
                                    ),
                                  )),
                              CountDownTimer(
                                datetime: vm.datetime,
                              ),
                            ],
                          )),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            prizeChip(vm.quantity),
                            Row(
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "At stake: ",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: Theme.of(context).primaryColor),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(right: 4),
                                  child: Text(
                                    formatTicketsValues(vm.productTickets),
                                    style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        color: Theme.of(context).primaryColor),
                                  ),
                                ),
                                Container(
                                  width: 24,
                                  height: 24,
                                  margin: EdgeInsets.only(right: 2.0),
                                  child: SvgPicture.asset(
                                      'assets/icons/tickets.svg'),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                    color: Colors.white,
                    margin: EdgeInsets.symmetric(vertical: 8.0),
                    child: Column(children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(16.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(bottom: 12.0),
                              child: Text(
                                "Description",
                                style: TextStyle(
                                    fontWeight: FontWeight.w700,
                                    color: Theme.of(context).primaryColor),
                              ),
                            ),
                            DescriptionTextWidget(text: vm.description),
                          ],
                        ),
                      ),
                    ])),
                Container(
                  margin: EdgeInsets.only(top: 16.0),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(16.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Participants",
                              style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                  color: Theme.of(context).primaryColor),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ]),
            ),
            ParticipantsList(),
            SizedBox(
              height: 42,
            )
          ],
        ),
      ),
      Positioned(
        bottom: 0.0,
        left: 0.0,
        right: 0.0,
        child: Container(
          height: 42.0,
          decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black38,
                  blurRadius: 12.0,
                  offset: Offset(0.0, 0.0))
            ],
            color: Colors.white,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            CircleAvatar(
                              radius: 14.0,
                              backgroundColor: Colors.transparent,
                              child: vm.userParticipation.image != null
                                  ? CachedNetworkImage(
                                      imageUrl: vm.userParticipation.image,
                                      errorWidget: (context, url, error) =>
                                          Image.asset(
                                              'assets/images/avatars/bot-4.png'),
                                    )
                                  : Image.asset(
                                      'assets/images/avatars/bot-4.png'),
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 8.0),
                              child: Text(
                                vm.userParticipation.username,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Theme.of(context).primaryColor),
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              child: Text(
                                formatTicketsValues(
                                    vm.userParticipation.tickets),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Theme.of(context).primaryColor),
                              ),
                            ),
                            Container(
                              width: 24,
                              height: 24,
                              margin: EdgeInsets.only(left: 4.0),
                              child:
                                  SvgPicture.asset('assets/icons/tickets.svg'),
                            ),
                          ],
                        )
                      ],
                    ),
                  )),
              Material(
                  color: Colors.deepPurple,
                  child: SizedBox(
                    width: 120.0,
                    height: double.infinity,
                    child: InkWell(
                      onTap: () => showDialog(
                        context: context,
                        builder: (BuildContext context) => TicketsDialog(
                          userTickets: vm.userTickets,
                          productId: vm.id,
                        ),
                      ),
                      child: Center(
                        child: Text(
                          "Participate",
                          style: TextStyle(
                              color: Colors.white, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ))
            ],
          ),
        ),
      )
    ]);
  }
}
