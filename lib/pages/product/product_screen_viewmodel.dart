import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:igotu/models/participant.dart';
import 'package:igotu/models/user.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:redux/redux.dart';

// ignore: prefer_double_quotes
part 'product_screen_viewmodel.g.dart';

abstract class ProductScreenViewModel
    implements Built<ProductScreenViewModel, ProductScreenViewModelBuilder> {
  String get id;

  String get name;

  String get description;

  String get coverUrl;

  BuiltList<String> get images;

  DateTime get datetime;

  num get quantity;

  int get productTickets;

  @nullable
  Participant get userParticipation;

  int get userTickets;

  ProductScreenViewModel._();

  factory ProductScreenViewModel(
          [void Function(ProductScreenViewModelBuilder) updates]) =
      _$ProductScreenViewModel;

  static ProductScreenViewModel fromStore(Store<AppState> store) {
    final product = store.state.product;
    final user = store.state.user;
    final participants = store.state.participants.toList();

    final userParticipation = _validateUserAsParticipant(participants, user);

    return ProductScreenViewModel((p) => p
      ..id = product.id
      ..name = product.name
      ..description = product.description
      ..coverUrl = product.coverUrl
      ..images = ListBuilder(product.images ?? [])
      ..datetime = product.timestamp
      ..quantity = product.quantity
      ..productTickets = product.tickets
      ..userParticipation = userParticipation.toBuilder()
      ..userTickets = user.tickets ?? 0);
  }

  //TODO: implement hasData

  static Participant _validateUserAsParticipant(
      List<Participant> participants, User user) {
    Participant defaultParticipant = Participant((p) => p
      ..uid = user.uid
      ..username = user.username
      ..image = user.image
      ..tickets = 0);
    if (participants.isEmpty) {
      return defaultParticipant;
    } else {
      final getUserFromList = participants.where((p) => p.uid == user.uid);

      if (getUserFromList.length == 0) {
        return defaultParticipant;
      } else {
        return getUserFromList.first;
      }
    }
  }
}
