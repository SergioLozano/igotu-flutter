import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:igotu/igotu_localization.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/authentication/auth_actions.dart';
import 'package:igotu/routes.dart';
import 'package:igotu/utils/logger.dart';
import 'package:igotu/widgets/common/animated_background.dart';
import 'package:igotu/widgets/common/progress.dart';

// ignore: must_be_immutable
class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => new _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  double _height, _fixedPadding;

  bool loading = false;

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _fixedPadding = _height * 0.025;

    final submitCallback = () {
      if (!loading) {
        final twitchLogInAction = TwitchLogIn();

        setState(() {
          loading = true;
        });

        StoreProvider.of<AppState>(context).dispatch(twitchLogInAction);

        twitchLogInAction.completer.future.catchError((error) {
          _scaffoldKey.currentState.hideCurrentSnackBar();
          setState(() {
            loading = false;
          });
          Logger.w(error.code.toString());
          _scaffoldKey.currentState.showSnackBar(SnackBar(
              backgroundColor: Colors.red[400],
              content: Text(
                IgotuLocalizations.of(context)
                    .authErrorMessage(error.code.toString()),
                style: TextStyle(fontWeight: FontWeight.bold),
              )));
        });
      }
    };
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.deepPurple[800],
        body: AnimatedBackground(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(_fixedPadding),
                  child: Material(
                    type: MaterialType.transparency,
                    elevation: 10.0,
                    child: Image.asset("assets/images/logo.png",
                        height: _height * 0.15),
                  ),
                ),
                SizedBox(height: _fixedPadding * 1.5),
                // AppName:
                Text(IgotuLocalizations.of(context).welcome,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 24.0,
                        fontWeight: FontWeight.w400)),
                SizedBox(height: _height * 0.33),
                Container(
                  constraints: BoxConstraints(maxWidth: 220.0, maxHeight: 48.0),
                  child: MaterialButton(
                    elevation: 15.0,
                    minWidth: double.infinity,
                    height: 48.0,
                    onPressed: submitCallback,
                    child: loading
                        ? Center(
                            child: CustomProgressIndicator(
                              color: Colors.white24,
                            ),
                          )
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(right: 16.0),
                                child: Icon(
                                  FontAwesome.twitch,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                IgotuLocalizations.of(context).twitchLogin,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w700),
                              ),
                            ],
                          ),
                    color: Color(0xFF9146FF),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                  ),
                ),

                SizedBox(
                  height: _fixedPadding,
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.phoneLogin);
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8.0)),
                  highlightColor: Colors.transparent,
                  child: Text(IgotuLocalizations.of(context).notTwitchLogin,
                      style: TextStyle(
                          color: Colors.deepPurple,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0)),
                ),
                SizedBox(
                  height: _fixedPadding,
                ),
                GestureDetector(
                  child: Column(
                    children: <Widget>[
                      Text(
                        IgotuLocalizations.of(context).termsHint,
                        style: TextStyle(
                            fontSize: 12.0,
                            color: Colors.deepPurple[900].withOpacity(0.8),
                            fontWeight: FontWeight.w600),
                      ),
                      SizedBox(
                        height: 2.0,
                      ),
                      Text(IgotuLocalizations.of(context).terms,
                          style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.deepPurple[900].withOpacity(0.95),
                              fontWeight: FontWeight.w700)),
                    ],
                  ),
                ),
                SizedBox(
                  height: _fixedPadding,
                ),
              ],
            ),
          ),
        ));
  }
}
