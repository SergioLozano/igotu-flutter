import 'package:built_value/built_value.dart';
import 'package:igotu/models/phone_auth.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:redux/redux.dart';

part 'phone_login_screen_viewmodel.g.dart';

abstract class PhoneLoginScreenViewModel
    implements
        Built<PhoneLoginScreenViewModel, PhoneLoginScreenViewModelBuilder> {
  PhoneAuth get phoneAuth;

  PhoneLoginScreenViewModel._();

  factory PhoneLoginScreenViewModel(
          [void Function(PhoneLoginScreenViewModelBuilder) updates]) =
      _$PhoneLoginScreenViewModel;

  static PhoneLoginScreenViewModel fromStore(Store<AppState> store) {
    return PhoneLoginScreenViewModel(
        (p) => p..phoneAuth = store.state.phoneAuth?.toBuilder());
  }
}
