// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_login_screen_viewmodel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PhoneLoginScreenViewModel extends PhoneLoginScreenViewModel {
  @override
  final PhoneAuth phoneAuth;

  factory _$PhoneLoginScreenViewModel(
          [void Function(PhoneLoginScreenViewModelBuilder) updates]) =>
      (new PhoneLoginScreenViewModelBuilder()..update(updates)).build();

  _$PhoneLoginScreenViewModel._({this.phoneAuth}) : super._() {
    if (phoneAuth == null) {
      throw new BuiltValueNullFieldError(
          'PhoneLoginScreenViewModel', 'phoneAuth');
    }
  }

  @override
  PhoneLoginScreenViewModel rebuild(
          void Function(PhoneLoginScreenViewModelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PhoneLoginScreenViewModelBuilder toBuilder() =>
      new PhoneLoginScreenViewModelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PhoneLoginScreenViewModel && phoneAuth == other.phoneAuth;
  }

  @override
  int get hashCode {
    return $jf($jc(0, phoneAuth.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PhoneLoginScreenViewModel')
          ..add('phoneAuth', phoneAuth))
        .toString();
  }
}

class PhoneLoginScreenViewModelBuilder
    implements
        Builder<PhoneLoginScreenViewModel, PhoneLoginScreenViewModelBuilder> {
  _$PhoneLoginScreenViewModel _$v;

  PhoneAuthBuilder _phoneAuth;
  PhoneAuthBuilder get phoneAuth =>
      _$this._phoneAuth ??= new PhoneAuthBuilder();
  set phoneAuth(PhoneAuthBuilder phoneAuth) => _$this._phoneAuth = phoneAuth;

  PhoneLoginScreenViewModelBuilder();

  PhoneLoginScreenViewModelBuilder get _$this {
    if (_$v != null) {
      _phoneAuth = _$v.phoneAuth?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PhoneLoginScreenViewModel other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$PhoneLoginScreenViewModel;
  }

  @override
  void update(void Function(PhoneLoginScreenViewModelBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PhoneLoginScreenViewModel build() {
    _$PhoneLoginScreenViewModel _$result;
    try {
      _$result = _$v ??
          new _$PhoneLoginScreenViewModel._(phoneAuth: phoneAuth.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'phoneAuth';
        phoneAuth.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'PhoneLoginScreenViewModel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
