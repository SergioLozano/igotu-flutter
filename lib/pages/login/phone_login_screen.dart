import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:igotu/data/country_respository.dart';
import 'package:igotu/igotu_localization.dart';
import 'package:igotu/models/country.dart';
import 'package:igotu/models/phone_auth.dart';
import 'package:igotu/pages/login/phone_login_screen_viewmodel.dart';
import 'package:igotu/redux/app_state.dart';
import 'package:igotu/redux/authentication/auth_actions.dart';
import 'package:igotu/utils/logger.dart';
import 'package:igotu/utils/utils.dart';
import 'package:igotu/widgets/auth/phone/phone_auth.dart';
import 'package:igotu/widgets/common/animated_background.dart';
import 'package:igotu/widgets/common/bubble.dart';
import 'package:igotu/widgets/common/progress.dart';
import 'package:pin_view/pin_view.dart';

class PhoneLoginScreen extends StatefulWidget {
  @override
  _PhoneLoginScreenState createState() => _PhoneLoginScreenState();
}

// ignore: must_be_immutable
class _PhoneLoginScreenState extends State<PhoneLoginScreen> {
  Color statusColor = Colors.deepPurple[700];

  /*
   *  _height & _width:
   *    will be calculated from the MediaQuery of widget's context
   *  countries:
   *    will be a list of Country model, Country model contains name, dialCode, flag and code for various countries
   *    and below params are all related to StreamBuilder
   */
  double _height, _width, _fixedPadding;
  String _hintText;
  List<Country> countries = [];
  StreamController<List<Country>> _countriesStreamController;
  Stream<List<Country>> _countriesStream;
  Sink<List<Country>> _countriesSink;

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController _searchCountryController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();

  FocusNode _phoneFieldFocus = FocusNode();

  /*
   *  This will be the index, we will modify each time the user selects a new country from the dropdown list(dialog),
   *  As a default case, we are using EEUU as default country, index = 234
   */
  int _selectedCountryIndex = 234; // TODO: Auto select country code.

  bool _showTooltip = false;

  bool _error = false;

  String _errorText;

  @override
  void initState() {
    super.initState();
    _phoneFieldFocus.addListener(_onFocusChange);
  }

  @override
  void dispose() {
    // While disposing the widget, we should close all the streams and controllers

    // Disposing Stream components
    //_countriesSink.close();
    //_countriesStreamController.close();

    // Disposing _countriesSearchController

    _searchCountryController.dispose();
    _phoneFieldFocus.dispose();

    super.dispose();
  }

  void _onFocusChange() {
    if (!_phoneFieldFocus.hasFocus) {
      setState(() {
        _showTooltip = false;
      });
    }
  }

  Future<List<Country>> loadCountriesJson() async {
    //  Cleaning up the countries list before we put our data in it
    countries.clear();

    String _userCountryCode = IgotuLocalizations.of(context).locale.countryCode;

    countries = codes
        .map((s) => Country((c) => c
          ..name = s['name']
          ..code = s['code']
          ..dialCode = s['dial_code']
          ..flag = s['flag']))
        .toList();

    int index = countries.indexWhere((c) => c.code == _userCountryCode);

    if (index >= 0) {
      setState(() {
        _selectedCountryIndex = index;
      });
    }
    return countries;
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    _fixedPadding = _height * 0.025;

    _hintText = IgotuLocalizations.of(context).userCountrySelect;

    if (countries.length < 240) {
      loadCountriesJson().whenComplete(() {
        final phoneAuth = PhoneAuth((p) => p..authStatus = AuthStatus.PHONE);
        StoreProvider.of<AppState>(context)
            .dispatch(PhoneAuthStatus(phoneAuth));
      });
    }

    return StoreConnector<AppState, PhoneLoginScreenViewModel>(
      distinct: true,
      converter: PhoneLoginScreenViewModel.fromStore,
      builder: (context, vm) {
        final status = vm.phoneAuth.authStatus;
        return Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              backgroundColor: Colors.deepPurple[900],
              elevation: 2,
              title: Text(IgotuLocalizations.of(context).loginOrSignupTitle,
                  style: TextStyle(color: Colors.white, fontSize: 18.0)),
            ),
            body: GestureDetector(
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: AnimatedBackground(
                    child: Center(
                        child: SingleChildScrollView(
                  child: Card(
                    color: statusColor,
                    elevation: 2.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                    child: Container(
                        //height: _height * 8 / 10,
                        width: _width * 8 / 10,
                        padding: EdgeInsets.all(_fixedPadding * 1.5),
                        child: _loginForm(status)),
                  ),
                )))));
      },
    );
  }

  Widget _loginForm(AuthStatus status) {
    switch (status) {
      case AuthStatus.PHONE:
        return _phoneForm();
        break;
      case AuthStatus.SMS:
        return _codeForm();
        break;
      case AuthStatus.LOADING:
        return CustomProgressIndicator(color: Colors.deepPurpleAccent);
        break;
      default:
        return CustomProgressIndicator(color: Colors.deepPurpleAccent);
        break;
    }
  }

  Widget _codeForm() => Column(
        children: <Widget>[
          Container(
            child: Text(
              "Waiting to automatically detect an SMS sent to your mobile number.",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 17.0,
                  fontWeight: FontWeight.w400,
                  color: Colors.white),
            ),
          ),
          SizedBox(
            height: 22.0,
          ),
          PinView(
            count: 6,
            margin: EdgeInsets.all(2.5),
            style: TextStyle(
                // style for the fields
                fontSize: 19.0,
                color: Colors.white,
                fontWeight: FontWeight.w500),
            inputDecoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(width: 2.25, color: Colors.white),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(width: 1.25, color: Colors.white),
              ),
              border: UnderlineInputBorder(
                  borderSide: BorderSide(width: 1.25, color: Colors.white)),
            ),
            submit: (String smsCode) {
              verifyCode(smsCode);
            },
          )
        ],
      );

  Widget _phoneForm() => Form(
        key: _formKey,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 0, left: 0),
              child: PhoneAuthWidgets.subTitle(
                  IgotuLocalizations.of(context).userCountrySelect),
            ),

            /*
           *  Select your country, this will be a custom DropDown menu, rather than just as a dropDown
           *  onTap of this, will show a Dialog asking the user to select country they reside,
           *  according to their selection, prefix will change in the PhoneNumber TextFormField
           */
            Padding(
              padding: EdgeInsets.only(left: 0, right: 0),
              child: PhoneAuthWidgets.selectCountryDropDown(
                  countries[_selectedCountryIndex], showCountries),
            ),

            //  Subtitle for Enter your phone
            Padding(
              padding: EdgeInsets.only(top: 10.0, left: 0),
              child: PhoneAuthWidgets.subTitle(
                  IgotuLocalizations.of(context).userEnterPhone),
            ),
            //  PhoneNumber TextFormFields
            Container(
                height: 70.0,
                padding:
                    EdgeInsets.only(left: 0, right: 0, bottom: _fixedPadding),
                child: Card(
                    child: Stack(
                  overflow: Overflow.visible,
                  children: <Widget>[
                    TextFormField(
                      controller: _phoneNumberController,
                      autofocus: true,
                      focusNode: _phoneFieldFocus,
                      onChanged: (value) {
                        setState(() {
                          _error = false;
                          _errorText = "";
                          _showTooltip = false;
                        });
                      },
                      validator: (value) {
                        setState(() {
                          _error = false;
                          _errorText = "";
                          _showTooltip = false;
                        });
                        if (value.isEmpty) {
                          setState(() {
                            _error = true;
                            _showTooltip = true;
                            _errorText =
                                IgotuLocalizations.of(context).emptyPhone;
                          });
                          return null;
                        }
                        String phoneNumber =
                            countries[_selectedCountryIndex].dialCode + value;
                        String errorText = validateMobile(phoneNumber);
                        if (errorText.isNotEmpty) {
                          setState(() {
                            _error = true;
                            _showTooltip = true;
                            _errorText =
                                IgotuLocalizations.of(context).validPhone;
                          });
                        }
                        return null;
                      },
                      keyboardType: TextInputType.phone,
                      key: Key('EnterPhone-TextFormField'),
                      style: TextStyle(
                          //fontSize: 14.0,
                          ),
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          contentPadding: const EdgeInsets.symmetric(
                              horizontal: 0.0, vertical: 10.0),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(4.0),
                              borderSide: BorderSide(
                                  width: 0, style: BorderStyle.none)),
                          errorMaxLines: 1,
                          prefixIcon: Padding(
                              padding: EdgeInsets.only(bottom: 2.0),
                              child: Center(
                                  widthFactor: 0.0,
                                  child: Text(
                                    "  " +
                                        countries[_selectedCountryIndex]
                                            .dialCode +
                                        "  ",
                                    style: TextStyle(
                                        color: Colors.black.withOpacity(0.5),
                                        fontWeight: FontWeight.w600,
                                        fontSize: 15.0),
                                  ))),
                          suffixIcon: _error
                              ? IconButton(
                                  icon: Icon(
                                    Icons.error,
                                    color: Colors.red[400],
                                    size: 21.0,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      _showTooltip = !_showTooltip;
                                    });
                                  },
                                )
                              : null),
                    ),
                    Positioned(
                      top: 38,
                      right: -25,
                      //You can use your own custom tooltip widget over here in place of below Container
                      child: _showTooltip
                          ? Bubble(
                              shadowColor: Colors.red,
                              elevation: 2,
                              color: Colors.red[400],
                              child: Center(
                                child: Text(
                                  _errorText,
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 12.0),
                                ),
                              ),
                            )
                          : Container(),
                    )
                  ],
                ))),

            /*
           *  Some informative text
           */
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(width: 4.0),
                //Icon(Icons.info, color: Colors.white, size: 20.0),
                SizedBox(width: 4.0),
                Expanded(
                  child: RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text: IgotuLocalizations.of(context)
                            .phoneVerificationHint,
                        style: TextStyle(
                            color: Colors.white, fontWeight: FontWeight.w400)),
                  ])),
                ),
                SizedBox(width: 4.0),
              ],
            ),

            /*
           *  Button: OnTap of this, it appends the dial code and the phone number entered by the user to send OTP,
           *  knowing once the OTP has been sent to the user - the user will be navigated to a new Screen,
           *  where is asked to enter the OTP he has received on his mobile (or) wait for the system to automatically detect the OTP
           */
            SizedBox(height: _fixedPadding * 1.5),
            RaisedButton(
              elevation: 15.0,
              onPressed: () {
                startPhoneAuth();
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  IgotuLocalizations.of(context).actionSendCode,
                  style: TextStyle(color: statusColor, fontSize: 18.0),
                ),
              ),
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)),
            ),
          ],
        ),
      );

  showCountries() {
    /*
     * Initialising components required for StreamBuilder
     * We will not be using _countriesStreamController anywhere, but just to initialize Stream & Sink from that
     * _countriesStream will give us the data what we need(output) - that will be used in StreamBuilder widget
     * _countriesSink is the place where we send the data(input)
     */
    _countriesStreamController = StreamController();
    _countriesStream = _countriesStreamController.stream;
    _countriesSink = _countriesStreamController.sink;
    _countriesSink.add(countries);

    showDialog(
      context: context,
      builder: (BuildContext context) => searchAndPickYourCountryHere(),
    );
    _searchCountryController.addListener(searchCountries);
  }

  /*
   *  This will be the listener for searching the query entered by user for their country, (dialog pop-up),
   *  searches for the query and returns list of countries matching the query by adding the results to the sink of _countriesStream
   */
  searchCountries() {
    String query = _searchCountryController.text;
    if (query.length == 0 || query.length == 1) {
      _countriesSink.add(countries);
      //print('added all countries again');
    } else if (query.length >= 2 && query.length <= 5) {
      List<Country> searchResults = [];
      searchResults.clear();
      countries.forEach((Country c) {
        if (c.toString().toLowerCase().contains(query.toLowerCase()))
          searchResults.add(c);
      });
      _countriesSink.add(searchResults);
      //print('added few countries based on search ${searchResults.length}');
    } else {
      //No results
      List<Country> searchResults = [];
      _countriesSink.add(searchResults);
      //print('no countries added');
    }
  }

  /*
   * Child for Dialog
   * Contents:
   *    SearchCountryTextFormField
   *    StreamBuilder
   *      - Shows a list of countries
   */
  Widget searchAndPickYourCountryHere() => WillPopScope(
        onWillPop: () => Future.value(true),
        child: Dialog(
          key: Key('SearchCountryDialog'),
          elevation: 8.0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          child: Container(
            margin: const EdgeInsets.all(5.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                //  TextFormField for searching country
                PhoneAuthWidgets.searchCountry(
                    _searchCountryController, _hintText),

                //  Returns a list of Countries that will change according to the search query
                SizedBox(
                  height: 300.0,
                  child: StreamBuilder<List<Country>>(
                      //key: Key('Countries-StreamBuilder'),
                      stream: _countriesStream,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          // print(snapshot.data.length);
                          return snapshot.data.length == 0
                              ? Center(
                                  child: Text(
                                      IgotuLocalizations.of(context)
                                          .searchNoResult,
                                      style: TextStyle(fontSize: 16.0)),
                                )
                              : ListView.builder(
                                  itemCount: snapshot.data.length,
                                  itemBuilder: (BuildContext context, int i) =>
                                      PhoneAuthWidgets.selectableWidget(
                                          snapshot.data[i],
                                          (Country c) =>
                                              selectThisCountry(c, i)),
                                );
                        } else if (snapshot.hasError)
                          return Center(
                            child: Text(IgotuLocalizations.of(context).error,
                                style: TextStyle(fontSize: 16.0)),
                          );
                        return Center(child: CircularProgressIndicator());
                      }),
                )
              ],
            ),
          ),
        ),
      );

  /*
   *  This callback is triggered when the user taps(selects) on any country from the available list in dialog
   *    Resets the search value
   *    Close the stream & sink
   *    Updates the selected Country and adds dialCode as prefix according to the user's selection
   */
  void selectThisCountry(Country country, int index) {
    _searchCountryController.clear();
    Navigator.of(context).pop();
    Future.delayed(Duration(milliseconds: 10)).whenComplete(() {
      _countriesStreamController.close();
      _countriesSink.close();

      setState(() {
        _selectedCountryIndex = countries.indexOf(country);
      });
    });
  }

  startPhoneAuth() {
    if (_formKey.currentState.validate() && !_error) {
      _phoneFieldFocus.unfocus();

      String phoneNumber = countries[_selectedCountryIndex].dialCode +
          _phoneNumberController.text;

      final loginAction = PhoneLogIn(phoneNumber: phoneNumber);

      StoreProvider.of<AppState>(context).dispatch(loginAction);

      loginAction.completer.future.catchError((error) {
        _scaffoldKey.currentState.hideCurrentSnackBar();
        Logger.w("Custom error" + error.code);
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            backgroundColor: Colors.red[400],
            content: Text(
              IgotuLocalizations.of(context)
                  .authErrorMessage(error.code.toString()),
              style: TextStyle(fontWeight: FontWeight.bold),
            )));
      });
    }
  }

  verifyCode(String smsCode) {
    final verifyAction = VerifyPhoneCode(smsCode: smsCode);
    StoreProvider.of<AppState>(context).dispatch(verifyAction);

    verifyAction.completer.future.catchError((error) {
      _scaffoldKey.currentState.hideCurrentSnackBar();
      Logger.w("Custom error" + error.code);
      _scaffoldKey.currentState.showSnackBar(SnackBar(
          backgroundColor: Colors.red[400],
          content: Text(
            IgotuLocalizations.of(context)
                .authErrorMessage(error.code.toString()),
            style: TextStyle(fontWeight: FontWeight.bold),
          )));
    });
  }
}
