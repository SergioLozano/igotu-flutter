import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:igotu/igotu_app.dart';
import 'package:igotu/utils/logger.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Firestore.instance
      .settings(
          timestampsInSnapshotsEnabled: true,
          persistenceEnabled: true,
          cacheSizeBytes: 2097152)
      .then((_) {
    print("Timestamps enabled in snapshots\n");
  }, onError: (e) {
    Logger.e("Error enabling timestamps in snapshots",
        e: e, s: StackTrace.current);
  });
  configureLogger();
  runApp(IgotuApp());
}
