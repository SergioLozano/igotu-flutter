import 'package:igotu/utils/logger.dart';

int getTicketsCount(tickets) {
  if (tickets == null) {
    return 0;
  }
  int count = 0;
  tickets.values.forEach((val) {
    if (val > 0) {
      count += val;
    }
  });
  return count;
}

formatTicketsValues(int tickets) {
  if (tickets == null) {
    return '0';
  }

  num count = tickets;

  if (count <= 999) {
    return count.toString();
  } else if (count > 999) {
    String absolute = ((count.abs() / 1000).round().toString()) + 'k';
    return absolute;
  } else if (count > 999999) {
    String absolute = ((count.abs() / 1000000).toStringAsFixed(1)) + 'M';
    return absolute;
  }
}

String validateMobile(String value) {
  Logger.d("Phone number: " + value);
  String pattern = r'^(?:[+0][0-9])?[0-9]{10,12}$';
  RegExp regExp = RegExp(pattern);

  if (value.length == 0) {
    return 'Please enter mobile number';
  } else if (!regExp.hasMatch(value)) {
    return 'Please enter valid mobile number';
  }
  return "";
}
